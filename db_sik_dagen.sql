-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 14, 2021 at 03:11 PM
-- Server version: 8.0.23
-- PHP Version: 7.3.19-1+eagle

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sik_dagen`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hsv`
--

CREATE TABLE `hsv` (
  `_id` int NOT NULL,
  `val` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `hsv`
--

INSERT INTO `hsv` (`_id`, `val`) VALUES
(1, 'satu.#dua.#tiga.#empat.#lima.'),
(2, 'ini#adalah#string'),
(3, 'ini#adalah#string'),
(4, 'ini#adalah#string'),
(5, 'ini#adalah#string#hsv'),
(6, NULL),
(7, NULL),
(8, 'ini#adalah#string#hsv jkjk mmm'),
(9, NULL),
(10, 'auah#ibu#untuk selolah A'),
(11, 'Jenis Pembuatan KTP : Baru'),
(12, '###############'),
(13, 'Paidi#Misiyem#untuk mendapatkan beasiswa di kampus A'),
(14, 'Selasa, 26 Januari 2021#Dangdut Koplo#750 Orang#Nikahan anak tersayang'),
(15, '01 Januari 1999#13:40 WIB#Sakit#RS Mangunnan#Dokter Paijo#Larno#Sulis#Sudjman#Agus#Kotak'),
(16, 'PT.XYZ#Dagen, Karanganyar'),
(17, 'Agus#3312837719380005#Melia Devana#Perempuan#Karanganyar#RS Soedjman#Selasa,02 Januari 2021#09:30#Sesar#Dokter Ajik#3,6 Kg#47 cm#Enny#Agus#Satpam#Suster'),
(18, 'Jenis Pembuatan KTP : Baru'),
(19, 'Selasa, 08 Mei 2021#Orkes Tunggal#321#Nikah Mertua'),
(20, 'Selasa,08 Februari 2021#Dangdutan#8000\n800#Ultah'),
(21, 'Aku#Kmu#untuk pindah de'),
(22, 'PT. ZXX#Dagen, Karangtengah'),
(23, 'Jenis Pembuatan KTP : Hilang'),
(24, 'CV. MN#Dagen, Mojosongo'),
(25, 'Lele#Karanganyar'),
(26, '02 Februari 2021#09:30#Sakit#RS A#Dokter A#Ayah#Ibu#Pelapor#Saksi I#Saksi II'),
(27, 'Jenis Pembuatan KTP : Baru'),
(28, 'Selasa, 09 Juni 1998#Orkes melayu#350#Hajat'),
(29, 'Jenis Pembuatan KTP : Rusak HVS'),
(30, 'Jenis Pembuatan KTP : Rusak HVS B'),
(31, 'Jenis Pembuatan KTP : Rusak HVS C'),
(32, 'Jenis Pembuatan KTP : Rusak HVS D'),
(33, 'Jenis Pembuatan KTP : Rusak HVS E'),
(34, 'Jenis Pembuatan KTP : Rusak HVS F'),
(35, 'Jenis Pembuatan KTP : Rusak HVS G'),
(36, 'Jenis Pembuatan KTP : Rusak HVS H'),
(37, 'Jenis Pembuatan KTP : Rusak HVS I'),
(38, 'Jenis Pembuatan KTP : Rusak HVS J'),
(39, 'Jenis Pembuatan KTP : Rusak HVS K'),
(40, 'Jenis Pembuatan KTP : Rusak HVS L'),
(41, 'Jenis Pembuatan KTP : Rusak HVS M'),
(42, 'Jenis Pembuatan KTP : Rusak HVS N'),
(43, 'Jenis Pembuatan KTP : Rusak HVS O'),
(44, 'Jenis Pembuatan KTP : Rusak HVS P'),
(45, 'Jenis Pembuatan KTP : Rusak HVS Q'),
(46, 'Jenis Pembuatan KTP : Rusak HVS R'),
(47, 'Jenis Pembuatan KTP : Rusak HVS T'),
(48, 'Jenis Pembuatan KTP : Rusak HVS U'),
(49, 'Jenis Pembuatan KTP : Rusak HVS V'),
(50, 'Jenis Pembuatan KTP : Rusak HVS W'),
(51, 'Jenis Pembuatan KTP : Rusak HVS X'),
(52, 'Jenis Pembuatan KTP : Rusak HVS Y'),
(53, 'Jenis Pembuatan KTP : Rusak HVS Z');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `table_alasan`
--

CREATE TABLE `table_alasan` (
  `_id` int NOT NULL,
  `alasan` text,
  `tgl_penolakan` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_alasan`
--

INSERT INTO `table_alasan` (`_id`, `alasan`, `tgl_penolakan`) VALUES
(10030, 'Tidak cocok dengan itu', '2021-02-12'),
(10031, 'Penolakan didasari oleh keperluan yang tidak mendasar', '2021-02-12'),
(10033, 'Maaf anda', '2021-02-12');

-- --------------------------------------------------------

--
-- Table structure for table `table_riwayat`
--

CREATE TABLE `table_riwayat` (
  `_id` int NOT NULL,
  `kode_transaksi` int DEFAULT NULL,
  `dikeluarkan` datetime DEFAULT NULL,
  `qr` text,
  `release_number` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_riwayat`
--

INSERT INTO `table_riwayat` (`_id`, `kode_transaksi`, `dikeluarkan`, `qr`, `release_number`) VALUES
(1, 10005, '2020-07-28 21:47:01', '\"null\"', 1),
(2, 10002, '2020-07-29 00:58:45', 'null', 1),
(3, 10018, '2021-01-11 00:00:00', 'dgfgfdg', 2),
(4, 10019, '2021-01-28 18:52:14', 'null', 1),
(5, 10025, '2021-02-07 14:29:09', 'null', 3),
(6, 10020, '2021-02-07 14:29:09', 'null', 1),
(7, 10021, '2021-02-07 14:29:09', 'null', 2),
(8, 10022, '2021-02-07 14:29:09', 'null', 3),
(9, 10023, '2021-02-07 14:29:09', 'null', 4);

-- --------------------------------------------------------

--
-- Table structure for table `table_rt`
--

CREATE TABLE `table_rt` (
  `_id` int NOT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `nik` varchar(16) DEFAULT NULL,
  `code` varchar(3) DEFAULT NULL,
  `rw` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='for rt identity';

--
-- Dumping data for table `table_rt`
--

INSERT INTO `table_rt` (`_id`, `nip`, `nik`, `code`, `rw`) VALUES
(10, '123', '100001', '2', NULL),
(12, '789', '123009', '7', 5),
(15, '1234', '123450', '6', 8);

-- --------------------------------------------------------

--
-- Table structure for table `table_rw`
--

CREATE TABLE `table_rw` (
  `_id` int NOT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `nik` varchar(16) DEFAULT NULL,
  `code` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `table_rw`
--

INSERT INTO `table_rw` (`_id`, `nip`, `nik`, `code`) VALUES
(1, '331201', '123450', '02'),
(2, '23222', '100001', '7'),
(3, '678', '123007', '5');

-- --------------------------------------------------------

--
-- Table structure for table `table_surat`
--

CREATE TABLE `table_surat` (
  `_id` int NOT NULL,
  `nama_surat` varchar(50) DEFAULT NULL,
  `format` varchar(50) DEFAULT NULL,
  `lasted` int DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_surat`
--

INSERT INTO `table_surat` (`_id`, `nama_surat`, `format`, `lasted`) VALUES
(9001, 'Surat Keterangan Tidak Mampu', '021/X/VII/2021', 1),
(9002, 'Surat Izin Hiburan', '022/X/VII/2021', 3),
(9003, 'Surat Keterangan Kematian', '023/X/VII/2021', 0),
(9004, 'Surat Keterangan Domisili Usaha', '024/X/VII/2021', 0),
(9005, 'Surat Keterangan Kelahiran', '025/X/VII/2021', 0),
(9006, 'Surat Pengantar KTP', '026/X/VII/2021', 0);

-- --------------------------------------------------------

--
-- Table structure for table `table_transaksi`
--

CREATE TABLE `table_transaksi` (
  `_id` int NOT NULL,
  `nik` varchar(16) DEFAULT NULL,
  `kode_surat` int DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `keperluan` text,
  `hsv` int DEFAULT NULL,
  `status` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_transaksi`
--

INSERT INTO `table_transaksi` (`_id`, `nik`, `kode_surat`, `created_date`, `keperluan`, `hsv`, `status`) VALUES
(10001, '1234567891234567', 9001, '2019-01-09', 'Untuk Melamar Kerja', 13, '1'),
(10002, '1234567891234567', 9002, '2019-01-02', 'Acara Nikahan', NULL, '1'),
(10003, '23432432432', 9001, '2020-01-16', 'Untuk Biasa', NULL, 'R'),
(10004, '23432432432', 9002, '2020-01-15', 'Acara Tawuran', NULL, 'K0'),
(10005, '1234567897654321', 9004, '2020-01-16', 'Membuat Surat Kematian atas nama someone', NULL, '1'),
(10006, '31', 9001, '2020-01-17', 'A', NULL, 'R'),
(10007, '123450', 9001, '2020-01-22', 'ase', NULL, 'R0'),
(10008, '123450', 9004, '2020-01-15', 'Aes', NULL, 'R'),
(10009, '123450', 9002, '2020-01-30', 'Saya', NULL, '1'),
(10010, '31', 9001, '2021-01-27', 'Coba HSV', 5, 'R'),
(10011, '102121', 9001, '2021-01-27', 'V2new', 6, 'R'),
(10012, '102121', 9001, '2021-01-27', 'mmmmnmmmm', 7, 'R'),
(10013, '31', 9001, '2021-01-27', 'Coba HSV 1', 8, 'R'),
(10014, '102121', 9001, '2021-01-27', 'bbbb', 9, 'R'),
(10015, '102121', 9001, '2021-01-27', 'ket', 10, 'R'),
(10016, '102121', 9006, '2021-01-28', 'Jenis Pembuatan KTP : Baru', 11, 'R'),
(10017, '102121', 9005, '2021-01-28', NULL, 12, 'R'),
(10018, '102121', 9001, '2021-01-28', 'Pembuatan SKTM untuk beasiswa', 13, '1'),
(10019, '102121', 9002, '2021-01-28', 'Pembuatan SIH untuk Nikah Anak', 14, '1'),
(10020, '102121', 9003, '2021-01-28', 'Pembuatan SKK', 15, '1'),
(10021, '102121', 9004, '2021-01-28', 'Pembuatan SKDU PT.XYZ', 16, '1'),
(10022, '102121', 9005, '2021-01-28', 'Kelahiran A.N Melia', 17, '1'),
(10023, '102121', 9006, '2021-01-28', 'Jenis Pembuatan KTP : Baru', 18, '1'),
(10024, '31', 9002, '2021-02-07', 'Usia A', 19, 'R'),
(10025, '123008', 9002, '2021-02-07', 'Hura hura', 20, '1'),
(10026, '123008', 9001, '2021-02-07', 'Sktm 1', 21, 'W'),
(10027, '123008', 9004, '2021-02-09', 'Pembuatan SKDU', 22, 'R'),
(10028, '123008', 9006, '2021-02-09', 'Jenis Pembuatan KTP : Hilang', 23, 'W'),
(10029, '123008', 9004, '2021-02-09', 'SKDU CV. MM', 24, 'W'),
(10030, '123008', 9004, '2021-02-09', 'Ternak Lele SKDU', 25, 'R0'),
(10031, '123008', 9003, '2021-02-09', 'Ningal', 26, 'W0'),
(10032, '123008', 9006, '2021-02-09', 'Jenis Pembuatan KTP : Baru', 27, 'R'),
(10033, '123008', 9002, '2021-02-12', 'Pembuatan Hajat', 28, 'R0'),
(10034, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak A', 29, 'R'),
(10035, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak B', 30, 'R'),
(10036, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak C', 31, 'R'),
(10037, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak D', 32, 'R'),
(10038, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak E', 33, 'R'),
(10039, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak F', 34, 'R'),
(10040, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak G', 35, 'R'),
(10041, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak H', 36, 'R'),
(10042, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak I', 37, 'R'),
(10043, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak J', 38, 'R'),
(10044, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak K', 39, 'R'),
(10045, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak L', 40, 'R'),
(10046, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak M', 41, 'R'),
(10047, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak N', 42, 'R'),
(10048, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak O', 43, 'R'),
(10049, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak P', 44, 'R'),
(10050, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak Q', 45, 'R'),
(10051, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak R', 46, 'R'),
(10052, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak T', 47, 'R'),
(10053, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak U', 48, 'R'),
(10054, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak V', 49, 'R'),
(10055, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak W', 50, 'R'),
(10056, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak X', 51, 'R'),
(10057, '123008', 9006, '2021-02-13', 'Jenis Pembuatan KTP : Rusak Y', 52, 'R'),
(10058, '123008', 9006, '2021-02-14', 'Jenis Pembuatan KTP : Rusak Z', 53, 'R');

-- --------------------------------------------------------

--
-- Table structure for table `table_warga`
--

CREATE TABLE `table_warga` (
  `_id` int NOT NULL,
  `nik` varchar(16) DEFAULT NULL,
  `kk` varchar(16) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `place_of_birth` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `religion` varchar(25) DEFAULT NULL,
  `rt` varchar(3) DEFAULT NULL,
  `rw` varchar(3) DEFAULT NULL,
  `gender` varchar(15) DEFAULT NULL,
  `job_status` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='this for about identity peoples in the village';

--
-- Dumping data for table `table_warga`
--

INSERT INTO `table_warga` (`_id`, `nik`, `kk`, `name`, `place_of_birth`, `birthday`, `address`, `religion`, `rt`, `rw`, `gender`, `job_status`) VALUES
(1, '1234567891234567', '1234567891234500', 'Abah Setiawan', NULL, '2020-07-24', 'Dangen, 022/21, Karanganyar', NULL, '2', '7', 'Laki-laki', NULL),
(2, '1234567897654321', '1234567897654300', 'Milea', NULL, '2020-07-15', 'Dangen, 21/21, Karanganyar', NULL, '2', NULL, 'Perempuan', NULL),
(3, '31', '21', 'Ani', NULL, '2020-07-20', 'ffew', NULL, '1', NULL, 'Laki-laki', NULL),
(4, '23432432432', '234322232323', 'Dea Nur Aviantika', NULL, '2020-07-17', 'Shdd 12,sosn,asda', NULL, '2', NULL, 'Perempuan', NULL),
(5, '123450', '1234500', 'Mila Anjani', 'Sukoharjo', '1998-02-03', 'Tanjung, Dagen, Selo, Karanganyar', 'Islam', '6', '8', 'Perempuan', 'Mahasiswa'),
(6, '100001', '110001', 'New Subject', NULL, '2020-11-04', 'Sukoharjo', NULL, '2', NULL, 'Laki-laki', NULL),
(7, '1234567897654321', '1234567897654300', 'Milea', NULL, '2020-07-15', 'Dangen, 21/21, Karanganyar', NULL, '2', NULL, 'Perempuan', NULL),
(9, '102121', '202121', 'UP', 'AOS', '2021-01-06', 'UP', 'ASD', '6', '8', 'Laki-laki', 'ASSS'),
(10, '123009', '1234009', 'Sugio', 'Sukoharjo', '1997-02-27', 'Dagen', 'Islam', '7', '5', 'Laki-laki', 'Swasta'),
(11, '123008', '1234008', 'Putri', 'Sukoharjo', '2000-09-11', 'Jeruk', 'Islam', '7', '5', 'Perempuan', 'Pelajar'),
(12, '123007', '1234007', 'Tomi', 'Sukoharjo', '2021-06-08', 'Salak', 'Islam', '7', '5', 'Laki-laki', 'Nganggur');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Wahyu Setiawan', 'admin@material.com', '2020-07-27 23:32:02', '$2y$10$/FPF5Sh73fkYvYVDiG2LQOAu4i4GDh9PNEdqgKlSe4gzY.G1n230C', 'GWUPULGvDIoDjw2P9Mr54sZHfDq83MDRwP6OIH6CQ8WFyBHcE4Stl1HhwKHY', '2020-07-27 23:32:02', '2020-07-28 18:30:40'),
(2, 'Wahyu Setiawan', 'admin@sik.so', NULL, '$2y$10$1sqneHlQbaS4IjZBeS21vuHBZN8f75B8D1DXf7wJcir6TXZyOuz1.', NULL, '2020-07-28 18:32:46', '2020-07-28 18:32:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hsv`
--
ALTER TABLE `hsv`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `table_alasan`
--
ALTER TABLE `table_alasan`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `table_riwayat`
--
ALTER TABLE `table_riwayat`
  ADD PRIMARY KEY (`_id`),
  ADD UNIQUE KEY `table_riwayat__id_uindex` (`_id`),
  ADD KEY `table_riwayat_table_transaksi__id_fk` (`kode_transaksi`);

--
-- Indexes for table `table_rt`
--
ALTER TABLE `table_rt`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `table_rw`
--
ALTER TABLE `table_rw`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `table_surat`
--
ALTER TABLE `table_surat`
  ADD PRIMARY KEY (`_id`),
  ADD UNIQUE KEY `table_surat__id_uindex` (`_id`);

--
-- Indexes for table `table_transaksi`
--
ALTER TABLE `table_transaksi`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `table_transaksi_table_surat__id_fk` (`kode_surat`);

--
-- Indexes for table `table_warga`
--
ALTER TABLE `table_warga`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `table_rt`
--
ALTER TABLE `table_rt`
  MODIFY `_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `table_rw`
--
ALTER TABLE `table_rw`
  MODIFY `_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `table_warga`
--
ALTER TABLE `table_warga`
  MODIFY `_id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `table_riwayat`
--
ALTER TABLE `table_riwayat`
  ADD CONSTRAINT `table_riwayat_table_transaksi__id_fk` FOREIGN KEY (`kode_transaksi`) REFERENCES `table_transaksi` (`_id`);

--
-- Constraints for table `table_transaksi`
--
ALTER TABLE `table_transaksi`
  ADD CONSTRAINT `table_transaksi_table_surat__id_fk` FOREIGN KEY (`kode_surat`) REFERENCES `table_surat` (`_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
