<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ReportController extends Controller
{

    public function index()
    {
        $data = DB::select('select
       table_transaksi._id as _id_transaksi,
       table_transaksi.keperluan as keperluan,
       table_transaksi.created_date as tgl_pengajuan,
       table_transaksi.status as status,
       ss._id as kode_surat,
       ss.nama_surat as nama_surat,
       ss.format as format,
       tw.nik as nik,
       tw.kk as kk,
       tw.name as name,
       tw.birthday as birthday,
       tw.address as address,
       tw.rt as rt,
       tw.gender as gender,
       tr._id as no_surat,
       tr.dikeluarkan as dikeluarkan,
       tr.qr as qr
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
    inner join table_warga tw on table_transaksi.nik = tw.nik
    left join table_riwayat tr on table_transaksi._id = tr.kode_transaksi
where status = ? order by _id_transaksi ASC ', array('1'));
        return view('report.report_list', ["data" => $data, "page" => "all"]);
    }

    //hari ini
    public function today()
    {
        $data = DB::select('select
       table_transaksi._id as _id_transaksi,
       table_transaksi.keperluan as keperluan,
       table_transaksi.created_date as tgl_pengajuan,
       table_transaksi.status as status,
       ss._id as kode_surat,
       ss.nama_surat as nama_surat,
       ss.format as format,
       tw.nik as nik,
       tw.kk as kk,
       tw.name as name,
       tw.birthday as birthday,
       tw.address as address,
       tw.rt as rt,
       tw.gender as gender,
       tr._id as no_surat,
       tr.dikeluarkan as dikeluarkan,
       tr.qr as qr
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
    inner join table_warga tw on table_transaksi.nik = tw.nik
    left join table_riwayat tr on table_transaksi._id = tr.kode_transaksi
where tr.dikeluarkan >= CURDATE() order by _id_transaksi ASC ', array('1'));
        return view('report.report_list', ["data" => $data, "page" => "today", "tgl" => $this->tgl_indo(date('Y-m-d'))]);
    }

    //minggu ini
    public function oneweek()
    {
        $data = DB::select('select
       table_transaksi._id as _id_transaksi,
       table_transaksi.keperluan as keperluan,
       table_transaksi.created_date as tgl_pengajuan,
       table_transaksi.status as status,
       ss._id as kode_surat,
       ss.nama_surat as nama_surat,
       ss.format as format,
       tw.nik as nik,
       tw.kk as kk,
       tw.name as name,
       tw.birthday as birthday,
       tw.address as address,
       tw.rt as rt,
       tw.gender as gender,
       tr._id as no_surat,
       tr.dikeluarkan as dikeluarkan,
       tr.qr as qr
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
    inner join table_warga tw on table_transaksi.nik = tw.nik
    left join table_riwayat tr on table_transaksi._id = tr.kode_transaksi
where tr.dikeluarkan >= CURDATE() - INTERVAL 7 DAY order by _id_transaksi ASC ', array('1'));
        return view('report.report_list', ["data" => $data, "page" => "oneweek", "tglawal" => $this->tgl_indo(date('Y-m-d',strtotime('-7 days'))), "tglakhir" => $this->tgl_indo(date('Y-m-d'))]);
    }


    public function details($id,$keperluan,$status,$kode_surat,$nama_surat,$nik,$kk,$name,$birthday,$address,$rt,$gender,$dikeluarkan,$qr,$format)
    {
        return view('report.details');
    }

    //fun help
    public static function tgl_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
}
