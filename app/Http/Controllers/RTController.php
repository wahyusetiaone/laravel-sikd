<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RTController extends Controller
{

    public function index()
    {
        $data = DB::select('select
 rt._id as _id,
 rt.nip as nip,
 rt.code as code,
 rt.rw as rw,
 tw.name as name,
 tw.address as address,
 tw.gender as gender
 from table_rt rt inner join table_warga tw on rt.nik = tw.nik');
        return view('rt.rt_list', ["data" => $data]);
    }

    public function form()
    {
        return view('rt.rt_form');
    }

    public function add(Request $request)
    {
        $nik = $request['nik'];
        $nip = $request['nip'];

        $data = DB::select('select rw, rt from table_warga where nik = ?', array($nik));

        $check = DB::select('select name from table_rt rt inner join table_warga warga on rt.nik = warga.nik where rt.code = ? and rt.rw = ?', array($data[0]->rt, $data[0]->rw));

        if (sizeof($check) == 0) {
            DB::select('insert into table_rt (nip,nik) values (? , ?)', array($nip, $nik));

            DB::select('update table_rt set rw = ?, code = ? where nip = ? ', array($data[0]->rw, $data[0]->rt, $nip));
            return redirect(route('rt'));
        } else {
            return view('rt.rt_form');
        }
    }

    public function find($nik)
    {
        $var = '%' . $nik . '%';
        $data = DB::select('select table_warga._id as _id, table_warga.nik as nik, table_warga.kk as kk, table_warga.name as name, table_warga.address as address, table_warga.gender as gender from table_warga left join table_rt on table_warga.nik = table_rt.nik where table_rt.nik is null and table_warga.nik like ?', array($var));
        return view('rt.citizen_for_rt_list', ["data" => $data]);
    }

    public function delete($nip)
    {
        DB::select('delete from table_rt where nip = ?', array($nip));

        return redirect(route('rt'));
    }
}
