<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HistoryController extends Controller
{

    public function index()
    {
        $data = DB::select('select
       table_transaksi._id as _id_transaksi,
       table_transaksi.keperluan as keperluan,
       table_transaksi.created_date as tgl_pengajuan,
       table_transaksi.status as status,
       ss._id as kode_surat,
       ss.nama_surat as nama_surat,
       ss.format as format,
       tw.nik as nik,
       tw.kk as kk,
       tw.name as name,
       tw.birthday as birthday,
       tw.address as address,
       tw.rt as rt,
       tw.gender as gender,
       tr._id as no_surat,
       tr.dikeluarkan as dikeluarkan,
       tr.qr as qr
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
    inner join table_warga tw on table_transaksi.nik = tw.nik
    left join table_riwayat tr on table_transaksi._id = tr.kode_transaksi
where ss._id is null or tw.nik is null or tr.kode_transaksi is null and status = ? or status = ? or status = ? order by _id_transaksi ASC ', array('K','K0','1'));
        return view('history.letter_list', ["data" => $data, "page" => "all"]);
    }

    //{i}/{k}/{st}/{ks}/{ns}/{nik}/{kk}/{n}/{b}/{a}/{r}/{g}
    public function confirm($id,$keperluan,$status,$kode_surat,$nama_surat,$nik,$kk,$name,$birthday,$address,$rt,$gender)
    {
        return view('history.form_confirmation');
    }

    public function details($id,$keperluan,$status,$kode_surat,$nama_surat,$nik,$kk,$name,$birthday,$address,$rt,$gender,$dikeluarkan,$qr,$format,$no)
    {
        return view('history.details');
    }

    public function update(Request $request)
    {
        $lasted = DB::select('select * from table_riwayat order by _id desc limit 1');
        if (empty($lasted)){
            $lasted_id = 0;
        }else{
            $lasted_id = $lasted[0]->_id + 1;
        }

        $kode = DB::select('select kode_surat from table_transaksi where _id = ?', array($request->id_transaksi));
        $kode_surat = $kode[0]->kode_surat;

        //1 = setuju, 0 = tdk setuju
        if ($request->status == 1){
            $lasteds = DB::select('select lasted from table_surat where _id = ?', array($kode_surat));
            $last = $lasteds[0]->lasted +1;

            DB::select('update table_transaksi set status = ? where _id = ?', array('1', $request->id_transaksi));
            DB::select('insert into table_riwayat (_id, kode_transaksi, dikeluarkan, qr, release_number) values (?,?,?,?,?)',array($lasted_id, $request->id_transaksi, now(), "null", $last));
            DB::select('update table_surat set lasted = ? where _id = ?', array($last, $kode_surat));
        }else{
            DB::select('update table_transaksi set status = ? where _id = ?', array('K0', $request->id_transaksi));
        }

        return redirect(route('history'));
    }

    //fun help
//    public static function tgl_indo($tanggal){
//        $bulan = array (
//            1 =>   'Januari',
//            'Februari',
//            'Maret',
//            'April',
//            'Mei',
//            'Juni',
//            'Juli',
//            'Agustus',
//            'September',
//            'Oktober',
//            'November',
//            'Desember'
//        );
//        $pecahkan = explode('-', $tanggal);
//
//        // variabel pecahkan 0 = tanggal
//        // variabel pecahkan 1 = bulan
//        // variabel pecahkan 2 = tahun
//
//        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
//    }
}
