<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SessionController extends Controller
{

    public function set(Request $request)
    {
        Session::put($request->setKey,$request->setValue);
    }

    public function get(Request $request)
    {
        return $request->session()->get($request->getKey);
    }

    public function forget(Request $request){
        Session::forget($request->fogetKey);
    }

}
