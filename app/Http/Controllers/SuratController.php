<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use DateTime;

class SuratController extends Controller
{

    public function sktm(Request $request){

        $no = $request->segment(2);

        $data = DB::select('select
       *
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
    inner join table_warga tw on table_transaksi.nik = tw.nik
    left join table_riwayat tr on table_transaksi._id = tr.kode_transaksi
    inner join hsv hs on table_transaksi.hsv = hs._id
where table_transaksi._id = ? ', array($no));

        $values = $this->log($data[0]->hsv);

        $dateS = new DateTime($data[0]->dikeluarkan);
        $d = $dateS->modify('+14 day');
        $str = $d->format('Y-m-d');

        return view('s_surat.s_sktm',['data'=>$data[0], 'values'=>$values, 'tgl_keluar'=> $this->tgl_indo(date('Y-m-d', strtotime($data[0]->dikeluarkan))), 'tgl_awal'=>$this->tgl_indo($str)]);
    }

    public function sip(Request $request){

        $no = $request->segment(2);

        $data = DB::select('select
       table_transaksi._id as _id_transaksi,
       table_transaksi.keperluan as keperluan,
       table_transaksi.status as status,
       table_transaksi.hsv as hsv,
       ss._id as kode_surat,
       ss.nama_surat as nama_surat,
       ss.format as format,
       tw.nik as nik,
       tw.kk as kk,
       tw.name as name,
       tw.birthday as birthday,
       tw.address as address,
       tw.rt as rt,
       tw.gender as gender,
       tw.job_status as job_status,
       tr.release_number as no_surat,
       tr.dikeluarkan as dikeluarkan,
       tr.qr as qr
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
    inner join table_warga tw on table_transaksi.nik = tw.nik
    left join table_riwayat tr on table_transaksi._id = tr.kode_transaksi
    inner join hsv hs on table_transaksi.hsv = hs._id
where table_transaksi._id = ? ', array($no));

        $values = $this->log($data[0]->hsv);

        return view('s_surat.s_sip',['data'=>$data[0], 'values'=>$values, 'tgl_keluar'=> $this->tgl_indo(date('Y-m-d ', strtotime($data[0]->dikeluarkan)))]);
    }

    public function skdu(Request $request){

        $no = $request->segment(2);

        $data = DB::select('select
       table_transaksi._id as _id_transaksi,
       table_transaksi.keperluan as keperluan,
       table_transaksi.status as status,
       table_transaksi.hsv as hsv,
       ss._id as kode_surat,
       ss.nama_surat as nama_surat,
       ss.format as format,
       tw.nik as nik,
       tw.kk as kk,
       tw.name as name,
       tw.birthday as birthday,
       tw.address as address,
       tw.religion as religion,
       tw.rt as rt,
       tw.gender as gender,
       tw.job_status as job_status,
       tr.release_number as no_surat,
       tr.dikeluarkan as dikeluarkan,
       tr.qr as qr
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
    inner join table_warga tw on table_transaksi.nik = tw.nik
    left join table_riwayat tr on table_transaksi._id = tr.kode_transaksi
    inner join hsv hs on table_transaksi.hsv = hs._id
where table_transaksi._id = ? ', array($no));


        $values = $this->log($data[0]->hsv);

        $dateS = new DateTime($data[0]->dikeluarkan);
        $d = $dateS->modify('+14 day');
        $str = $d->format('Y-m-d');


        return view('s_surat.s_skdu',['data'=>$data[0], 'values'=>$values, 'tgl_keluar'=> $this->tgl_indo(date('Y-m-d', strtotime($data[0]->dikeluarkan))), 'tgl_awal'=>$this->tgl_indo($str)]);
    }

    public function skk(Request $request){

        $no = $request->segment(2);

        $data = DB::select('select
       table_transaksi._id as _id_transaksi,
       table_transaksi.keperluan as keperluan,
       table_transaksi.status as status,
       table_transaksi.hsv as hsv,
       ss._id as kode_surat,
       ss.nama_surat as nama_surat,
       ss.format as format,
       tw.nik as nik,
       tw.kk as kk,
       tw.name as name,
       tw.birthday as birthday,
       tw.place_of_birth as placeofbirth,
       tw.address as address,
       tw.religion as religion,
       tw.rt as rt,
       tw.gender as gender,
       tw.job_status as job_status,
       tr.release_number as no_surat,
       tr.dikeluarkan as dikeluarkan,
       tr.qr as qr
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
    inner join table_warga tw on table_transaksi.nik = tw.nik
    left join table_riwayat tr on table_transaksi._id = tr.kode_transaksi
    inner join hsv hs on table_transaksi.hsv = hs._id
where table_transaksi._id = ? ', array($no));

        $values = $this->log($data[0]->hsv);

        return view('s_surat.s_skk',['data'=>$data[0], 'values'=>$values, 'tgl_keluar'=> $this->tgl_indo(date('Y-m-d', strtotime($data[0]->dikeluarkan)))]);
    }

    public function skl(Request $request){

        $no = $request->segment(2);

        $data = DB::select('select
       table_transaksi._id as _id_transaksi,
       table_transaksi.keperluan as keperluan,
       table_transaksi.status as status,
       table_transaksi.hsv as hsv,
       ss._id as kode_surat,
       ss.nama_surat as nama_surat,
       ss.format as format,
       tw.nik as nik,
       tw.kk as kk,
       tw.name as name,
       tw.birthday as birthday,
       tw.place_of_birth as placeofbirth,
       tw.address as address,
       tw.religion as religion,
       tw.rt as rt,
       tw.gender as gender,
       tw.job_status as job_status,
       tr.release_number as no_surat,
       tr.dikeluarkan as dikeluarkan,
       tr.qr as qr
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
    inner join table_warga tw on table_transaksi.nik = tw.nik
    left join table_riwayat tr on table_transaksi._id = tr.kode_transaksi
    inner join hsv hs on table_transaksi.hsv = hs._id
where table_transaksi._id = ? ', array($no));

        $values = $this->log($data[0]->hsv);

        return view('s_surat.s_skl',['data'=>$data[0], 'values'=>$values, 'tgl_keluar'=> $this->tgl_indo(date('Y-m-d', strtotime($data[0]->dikeluarkan))),]);
    }

    public function skktp(Request $request){

        $no = $request->segment(2);

        $data = DB::select('select
       table_transaksi._id as _id_transaksi,
       table_transaksi.keperluan as keperluan,
       table_transaksi.status as status,
       table_transaksi.hsv as hsv,
       ss._id as kode_surat,
       ss.nama_surat as nama_surat,
       ss.format as format,
       tw.nik as nik,
       tw.kk as kk,
       tw.name as name,
       tw.birthday as birthday,
       tw.place_of_birth as placeofbirth,
       tw.address as address,
       tw.religion as religion,
       tw.rt as rt,
       tw.gender as gender,
       tw.job_status as job_status,
       tr.release_number as no_surat,
       tr.dikeluarkan as dikeluarkan,
       tr.qr as qr
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
    inner join table_warga tw on table_transaksi.nik = tw.nik
    left join table_riwayat tr on table_transaksi._id = tr.kode_transaksi
    inner join hsv hs on table_transaksi.hsv = hs._id
where table_transaksi._id = ? ', array($no));

        $values = $this->log($data[0]->hsv);

        $dateS = new DateTime($data[0]->dikeluarkan);
        $d = $dateS->modify('+14 day');
        $str = $d->format('Y-m-d');

        return view('s_surat.s_skktp',['data'=>$data[0], 'values'=>$values, 'tgl_keluar'=> $this->tgl_indo(date('Y-m-d', strtotime($data[0]->dikeluarkan))), 'tgl_awal'=>$this->tgl_indo($str)]);
    }

    function log($id){
        $data = DB::select('select hsv.val from hsv where hsv._id = ?', array($id));
        $char = substr_count($data[0]->val,"#");
        $split = explode ("#", $data[0]->val);
        return array(['count_char'=>$char, 'split'=>$split]);
    }

    //fun help
    function tgl_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }
}
