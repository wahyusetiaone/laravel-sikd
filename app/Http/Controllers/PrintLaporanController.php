<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PrintLaporanController extends Controller
{
    public function index()
    {
        $sktm = self::allquery(9001);
        $sip = self::allquery(9002);
        $skdu = self::allquery(9004);
        $skk = self::allquery(9003);
        $skl = self::allquery(9005);
        $skktp = self::allquery(9006);
        return view('p_laporan.laporan', ["sktm" => $sktm, "sip" => $sip, "skdu" => $skdu, "skk" => $skk, "skl" => $skl, "skktp" => $skktp, "page" => "all"]);
    }

    //hari ini
    public function today()
    {
        $sktm = self::today_query(9001);
        $sip = self::today_query(9002);
        $skdu = self::today_query(9004);
        $skk = self::today_query(9003);
        $skl = self::today_query(9005);
        $skktp = self::today_query(9006);
        return view('p_laporan.laporan', ["sktm" => $sktm, "sip" => $sip, "skdu" => $skdu, "skk" => $skk, "skl" => $skl, "skktp" => $skktp, "page" => "today", "tgl" => $this->tgl_indo(date('Y-m-d'))]);
    }

    //minggu ini
    public function oneweek()
    {
        $sktm = self::week_query(9001);
        $sip = self::week_query(9002);
        $skdu = self::week_query(9004);
        $skk = self::week_query(9003);
        $skl = self::week_query(9005);
        $skktp = self::week_query(9006);
        return view('p_laporan.laporan', ["sktm" => $sktm, "sip" => $sip, "skdu" => $skdu, "skk" => $skk, "skl" => $skl, "skktp" => $skktp, "page" => "oneweek", "tglawal" => $this->tgl_indo(date('Y-m-d',strtotime('-7 days'))), "tglakhir" => $this->tgl_indo(date('Y-m-d'))]);
    }

    //fun help
    public static function tgl_indo($tanggal){
        $bulan = array (
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $tanggal);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun

        return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
    }

    public static function allquery($kode_surat){
       return $data = DB::select('select
       table_transaksi._id as _id_transaksi,
       table_transaksi.keperluan as keperluan,
       table_transaksi.created_date as tgl_pengajuan,
       table_transaksi.status as status,
       ss._id as kode_surat,
       ss.nama_surat as nama_surat,
       ss.format as format,
       tw.nik as nik,
       tw.kk as kk,
       tw.name as name,
       tw.birthday as birthday,
       tw.address as address,
       tw.rt as rt,
       tw.gender as gender,
       tr._id as no_surat,
       tr.release_number as num,
       tr.dikeluarkan as dikeluarkan
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
    inner join table_warga tw on table_transaksi.nik = tw.nik
    left join table_riwayat tr on table_transaksi._id = tr.kode_transaksi
where status = ? and kode_surat = ? order by _id_transaksi ASC ', array('1', $kode_surat));
    }

    public static function today_query($kode_surat){
        return DB::select('select
       table_transaksi._id as _id_transaksi,
       table_transaksi.keperluan as keperluan,
       table_transaksi.created_date as tgl_pengajuan,
       table_transaksi.status as status,
       ss._id as kode_surat,
       ss.nama_surat as nama_surat,
       ss.format as format,
       tw.nik as nik,
       tw.kk as kk,
       tw.name as name,
       tw.birthday as birthday,
       tw.address as address,
       tw.rt as rt,
       tw.gender as gender,
       tr._id as no_surat,
       tr.release_number as num,
       tr.dikeluarkan as dikeluarkan,
       tr.qr as qr
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
    inner join table_warga tw on table_transaksi.nik = tw.nik
    left join table_riwayat tr on table_transaksi._id = tr.kode_transaksi
where tr.dikeluarkan >= CURDATE() and kode_surat = ? order by _id_transaksi ASC ', array($kode_surat));
    }

    public static function week_query($kode_surat){
        return DB::select('select
       table_transaksi._id as _id_transaksi,
       table_transaksi.keperluan as keperluan,
       table_transaksi.created_date as tgl_pengajuan,
       table_transaksi.status as status,
       ss._id as kode_surat,
       ss.nama_surat as nama_surat,
       ss.format as format,
       tw.nik as nik,
       tw.kk as kk,
       tw.name as name,
       tw.birthday as birthday,
       tw.address as address,
       tw.rt as rt,
       tw.gender as gender,
       tr._id as no_surat,
       tr.release_number as num,
       tr.dikeluarkan as dikeluarkan,
       tr.qr as qr
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
    inner join table_warga tw on table_transaksi.nik = tw.nik
    left join table_riwayat tr on table_transaksi._id = tr.kode_transaksi
where tr.dikeluarkan >= CURDATE() - INTERVAL 7 DAY and kode_surat = ? order by _id_transaksi ASC ', array($kode_surat));
    }
}
