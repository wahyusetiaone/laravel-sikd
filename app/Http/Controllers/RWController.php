<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class RWController extends Controller
{

    public function index()
    {
        $data = DB::select('select * from table_rw inner join table_warga on table_rw.nik = table_warga.nik');
        return view('rw.rw_list', ["data" => $data]);
    }

    public function form()
    {
        return view('rw.rw_form');
    }

    public function add(Request $request)
    {
        DB::select('insert into table_rw (nip,nik,code) values ( ? , ? , ?)', array($request['nip'], $request['nik'], $request['code']));

        return redirect(route('rw'));
    }

    public function find($nik){
        $var = '%'.$nik.'%';
        $data = DB::select('select table_warga._id as _id, table_warga.nik as nik, table_warga.kk as kk, table_warga.name as name, table_warga.address as address, table_warga.gender as gender from table_warga left join table_rw on table_warga.nik = table_rw.nik where table_rw.nik is null and table_warga.nik like ?',array($var));
        return view('rw.citizen_for_rw_list', ["data" => $data]);
    }

    public function delete($nip)
    {
        DB::select('delete from table_rw where nip = ?', array($nip));

        return redirect(route('rw'));
    }
}
