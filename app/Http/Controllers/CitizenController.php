<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class CitizenController extends Controller
{

    public function index()
    {
        $data = DB::select('select * from table_warga');
        return view('warga.citizen_list', ["data" => $data]);
    }

    public function form()
    {
        return view('warga.citizen_form');
    }

    public function add(Request $request)
    {

        DB::select('insert into table_warga (nik,kk,name,place_of_birth,birthday, address, religion, rt, rw, gender, job_status) values ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?)', array($request['nik'], $request['kk'], $request['name'], $request['place_of_birth'], $request['birthday'], $request['address'], $request['religion'], $request['rt'], $request['rw'], $request['gender'], $request['job_status']));

        return redirect(route('warga'));
    }

    public function show($nik)
    {
        $data = DB::select('select * from table_warga where nik = ?', array($nik));

        return view('warga.show', ["data" => $data[0]]);
    }

    public function update(Request $request)
    {

        DB::select('update table_warga set kk = ?,name = ? , place_of_birth =? ,birthday = ? , address = ?, religion = ? , rt = ?, rw =? , gender = ?, job_status = ? where nik = ?', array($request['kk'], $request['name'], $request['place_of_birth'], $request['birthday'], $request['address'], $request['religion'], $request['rt'], $request['rw'], $request['gender'], $request['job_status'], $request['nik']));

        return redirect(route('warga'));
    }

    public function delete($nik)
    {
        DB::select('delete from table_warga where nik = ?', array($nik));

        return redirect(route('warga'));
    }
}
