<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HandlerEventPusher extends Controller
{
    public function eventRt($rt, $rw, $nik, $nama, $kode_surat , $nama_surat, $id_transaksi){

        $pusher = new Pusher( "3333c813add5c2d7360f", "e9e05f75aa3d5f815399", "1155280", array( 'cluster' => "ap1", 'useTLS' => true ) );

        $val = array([
            'rt' => $rt,
            'rw' => $rw,
            'nik' => $nik,
            'nama' => $nama,
            'kode_surat' => $kode_surat,
            'nama_surat' => $nama_surat,
            'id_transaksi' => $id_transaksi
        ]);
        $data = array('data' => $val);

        $pusher->trigger( 'channel-rt', 'rt_event', $data );

    }

    public function eventRw($rw, $nik, $nama, $kode_surat, $nama_surat, $id_transaksi){

        $pusher = new Pusher( "3333c813add5c2d7360f", "e9e05f75aa3d5f815399", "1155280", array( 'cluster' => "ap1", 'useTLS' => true ) );
        $pusher->trigger( 'channel-rw', 'rw_event', 'hello world' );

    }

    public function eventWarga($nik, $kode_surat , $nama_surat, $id_transaksi, $status){

        $pusher = new Pusher( "3333c813add5c2d7360f", "e9e05f75aa3d5f815399", "1155280", array( 'cluster' => "ap1", 'useTLS' => true ) );
        $pusher->trigger( 'warga-channel', 'warga_event', 'hello world' );

    }
}
