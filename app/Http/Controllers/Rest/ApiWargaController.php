<?php

namespace App\Http\Controllers\Rest;

use App\Events\SendGlobalNotificationRt;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Pusher\Pusher;

class ApiWargaController extends Controller
{
    public function login(Request $request)
    {
        $data = DB::select('select * from table_warga where kk = ?', array($request->kk));
        $data2 = DB::select('select * from table_warga where nik = ?', array($request->nik));

        if (sizeof($data) == 0) {
            return response()->json([
                'status' => false,
                'message' => "Nomor KK tidak terdaftrar !!!",
                'results' => array(
                    [
                        'kk' => $request->kk,
                        'nik' => $request->nik
                    ]
                )
            ])->header('Content-Type', 'application/json');

        } else {
            if (sizeof($data2) == 0) {
                return response()->json([
                    'status' => false,
                    'message' => "Nomor NIK tidak terdaftar !!!",
                    'results' => array(
                        [
                            'kk' => $request->kk,
                            'nik' => $request->nik
                        ]
                    )
                ])->header('Content-Type', 'application/json');

            } else {
                return response()->json([
                    'status' => true,
                    'message' => "Login successfully",
                    'results' => array(
                        [
                            'kk' => $request->kk,
                            'nik' => $request->nik
                        ]
                    )
                ])->header('Content-Type', 'application/json');
            }
        }

    }

    public function get(Request $request)
    {
        $data = DB::select('select * from table_warga where nik = ? ', array($request->nik));

        if (sizeof($data) == 0) {
            return response()->json([
                'status' => false,
                'message' => "Failed getting data",
                'results' => $data
            ])->header('Content-Type', 'application/json');

        } else {
            return response()->json([
                'status' => true,
                'message' => "Successfully getting data",
                'results' => $data
            ])->header('Content-Type', 'application/json');
        }
    }

    public function history(Request $request)
    {
        $data = DB::select('select
    table_transaksi._id as _id_transaksi,
    table_transaksi.keperluan as keperluan,
    table_transaksi.status as status,
    ss._id as kode_surat,
    ss.nama_surat as nama_surat,
    ss.format as format,
    tw.nik as nik,
    tw.kk as kk,
    tw.name as name,
    tw.birthday as birthday,
    tw.address as address,
    tw.rt as rt,
    tw.gender as gender,
    ta.alasan as alasan
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
                     left join table_warga tw on table_transaksi.nik = tw.nik
                     left join table_alasan ta on table_transaksi._id = ta._id
where tw.nik = ? order by _id_transaksi ASC', array($request->nik));
        if (sizeof($data) == 0) {
            return response()->json([
                'status' => false,
                'message' => "No history letter found !!!",
                'results' => $data
            ])->header('Content-Type', 'application/json');

        } else {
            return response()->json([
                'status' => true,
                'message' => "Successfully getting history letter",
                'results' => $data
            ])->header('Content-Type', 'application/json');
        }
    }

    public function request(Request $request)
    {
        $lasted = DB::select('select _id as id from table_transaksi order by _id desc limit 1');
        $_id = $lasted[0]->id + 1;

        $data = DB::select('insert into table_transaksi (_id, nik, kode_surat,created_date, keperluan, status) values (?,?,?,?,?,?)', array($_id, $request->nik, $request->surat, date('Y-m-d'), $request->keperluan, 'R'));
        return response()->json([
            'status' => true,
            'message' => "Successfully send the letter",
            'results' => array(
                ['code' => $_id]
            )
        ])->header('Content-Type', 'application/json');

    }

    public function requestv2(Request $request)
    {
        $lasted = DB::select('select _id as id from table_transaksi order by _id desc limit 1');
        $_id = $lasted[0]->id + 1;

        $hsvlasted = DB::select('select _id as id from hsv order by _id desc limit 1');
        $hsv_id = $hsvlasted[0]->id + 1;

        $call = DB::table('hsv')->insert([
            '_id' => $hsv_id,
            'val' => $request->hsv
        ]);

        if ($call) {
            DB::select('insert into table_transaksi (_id, nik, kode_surat,created_date, keperluan, hsv, status) values (?,?,?,?,?,?,?)', array($_id, $request->nik, $request->surat, date('Y-m-d'), $request->keperluan, $hsv_id, 'R'));
            $this->triggerEvent($_id);
        } else {
            return response()->json([
                'status' => false,
                'message' => "Failed send the letter",
                'results' => array(
                    ['code' => $_id]
                )
            ])->header('Content-Type', 'application/json');
        }

        return response()->json([
            'status' => true,
            'message' => "Successfully send the letter",
            'results' => array(
                ['code' => $_id]
            )
        ])->header('Content-Type', 'application/json');

    }

    private function triggerEvent($id)
    {

        $data = DB::select('select
    ss._id as kode_surat,
    ss.nama_surat as nama_surat,
    tw.nik as nik,
    tw.name as name,
    tw.rt as rt,
    tw.rw as rw
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
                     left join table_warga tw on table_transaksi.nik = tw.nik
                     left join table_alasan ta on table_transaksi._id = ta._id
where table_transaksi._id= ?', array($id));

        $pusher = new Pusher( "3333c813add5c2d7360f", "e9e05f75aa3d5f815399", "1155280", array( 'cluster' => "ap1", 'useTLS' => true ) );

        $data = $data[0]->rt.",".$data[0]->rw.",".$data[0]->nik.",".$data[0]->name.",".$data[0]->kode_surat.",".$data[0]->nama_surat.",".$id;

        $pusher->trigger( 'channel-rt', 'rt-event', $data );
    }

}
