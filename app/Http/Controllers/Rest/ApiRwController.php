<?php

namespace App\Http\Controllers\Rest;

use App\Events\SendGlobalNotificationWarga;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Pusher\Pusher;

class ApiRwController extends Controller
{
    public function login(Request $request)
    {
        $data = DB::select('select * from table_rw where nip = ? ', array($request->nip));

        if (sizeof($data) == 0) {
            return response()->json([
                'status' => false,
                'message' => "Login failed",
                'results' => array(
                    ['nip' => $request->nip]
                )
            ])->header('Content-Type', 'application/json');

        } else {
            return response()->json([
                'status' => true,
                'message' => "Login successfully",
                'results' => array(
                    ['nip' => $request->nip]
                )
            ])->header('Content-Type', 'application/json');
        }

    }

    public function get(Request $request)
    {
        $data = DB::select('select * from table_rw inner join table_warga on table_rw.nik = table_warga.nik where nip = ? ', array($request->nip));

        if (sizeof($data) == 0) {
            return response()->json([
                'status' => false,
                'message' => "Failed getting data",
                'results' => $data
            ])->header('Content-Type', 'application/json');

        } else {
            return response()->json([
                'status' => true,
                'message' => "Successfully getting data",
                'results' => $data
            ])->header('Content-Type', 'application/json');
        }
    }

    public function history(Request $request)
    {
        $data = DB::select('select
       table_transaksi._id as _id_transaksi,
       table_transaksi.keperluan as keperluan,
       table_transaksi.status as status,
       ss._id as kode_surat,
       ss.nama_surat as nama_surat,
       ss.format as format,
       tw.nik as nik,
       tw.kk as kk,
       tw.name as name,
       tw.birthday as birthday,
       tw.address as address,
       tw.rw as rw,
       tw.gender as gender
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
    right join table_warga tw on table_transaksi.nik = tw.nik
where rt = ? and status = ? or rw = ? and status = ? or rw = ? and status = ? or rw = ? and status = ? order by _id_transaksi ASC', array($request->rw, 'W0', $request->rw, 'K', $request->rw, 'K0', $request->rw, '1'));
        if (sizeof($data) == 0) {
            return response()->json([
                'status' => false,
                'message' => "No queue for incoming letter !!!",
                'results' => $data
            ])->header('Content-Type', 'application/json');

        } else {
            return response()->json([
                'status' => true,
                'message' => "Successfully getting history letter",
                'results' => $data
            ])->header('Content-Type', 'application/json');
        }
    }

    public function historyWaiting(Request $request)
    {
        $data = DB::select('select
           table_transaksi._id as _id_transaksi,
           table_transaksi.keperluan as keperluan,
           table_transaksi.status as status,
           ss._id as kode_surat,
           ss.nama_surat as nama_surat,
           ss.format as format,
           tw.nik as nik,
           tw.kk as kk,
           tw.name as name,
           tw.birthday as birthday,
           tw.address as address,
           tw.rw as rw,
           tw.gender as gender
    from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
        right join table_warga tw on table_transaksi.nik = tw.nik
    where rw = ? and status = ? order by _id_transaksi ASC', array($request->rw, 'W'));
        if (sizeof($data) == 0) {
            return response()->json([
                'status' => false,
                'message' => "No queue for incoming letter !!!",
                'results' => $data
            ])->header('Content-Type', 'application/json');

        } else {
            return response()->json([
                'status' => true,
                'message' => "Successfully getting history letter",
                'results' => $data
            ])->header('Content-Type', 'application/json');
        }
    }

    public function update(Request $request)
    {
        if ($request->status == "K") {
            DB::select('update table_transaksi set status = ? where _id = ?', array('K', $request->id_transaksi));
            return response()->json([
                'status' => true,
                'message' => "Successfully change the status of letter",
                'results' => array(
                    ['id_transaksi' => $request->id_transaksi, 'status' => 'K']
                )
            ])->header('Content-Type', 'application/json');
            $this->triggerEvent($request->id_transaksi, "K");
        } else {
            DB::select('update table_transaksi set status = ? where _id = ?', array('W0', $request->id_transaksi));
            DB::select('insert into table_alasan (_id, alasan, tgl_penolakan) values (?,?,?)', array($request->id_transaksi, $request->alasan, date('Y-m-d')));
            return response()->json([
                'status' => false,
                'message' => "Successfully change the status of letter",
                'results' => array(
                    ['id_transaksi' => $request->id_transaksi, 'status' => 'W0']
                )
            ])->header('Content-Type', 'application/json');
            $this->triggerEvent($request->id_transaksi, "W0");
        }
    }

    private function triggerEvent($id_transaksi, $status)
    {
        $data = DB::select('select
    ss._id as kode_surat,
    ss.nama_surat as nama_surat,
    tw.nik as nik,
    tw.name as name,
    tw.rt as rt,
    tw.rw as rw,
    table_transaksi.status as status
from table_transaksi left join table_surat ss on table_transaksi.kode_surat = ss._id
                     left join table_warga tw on table_transaksi.nik = tw.nik
                     left join table_alasan ta on table_transaksi._id = ta._id
where table_transaksi._id= ?', array($id_transaksi));

        $pusher = new Pusher( "3333c813add5c2d7360f", "e9e05f75aa3d5f815399", "1155280", array( 'cluster' => "ap1", 'useTLS' => true ) );

        $data = $data[0]->nik.",".$data[0]->kode_surat.",".$data[0]->nama_surat.",".$id_transaksi.",".$status;

        $pusher->trigger( 'channel-warga', 'warga-event', $data );

    }
}
