<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendGlobalNotificationWarga implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($nik, $kode_surat , $nama_surat, $id_transaksi, $status)
    {
        $this->nik = $nik;
        $this->kode_surat = $kode_surat;
        $this->nama_surat = $nama_surat;
        $this->id_transaksi = $id_transaksi;
        $this->status = $status;
    }

    public function broadcastOn()
    {
        return ['channel-warga'];
    }

    public function broadcastAs()
    {
        return 'warga-event';
    }
}
