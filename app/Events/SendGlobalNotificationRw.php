<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SendGlobalNotificationRw implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($rw, $nik, $nama, $kode_surat, $nama_surat, $id_transaksi)
    {
        $this->rw = $rw;
        $this->nik = $nik;
        $this->nama = $nama;
        $this->kode_surat = $kode_surat;
        $this->nama_surat = $nama_surat;
        $this->id_transaksi = $id_transaksi;
    }

    public function broadcastOn()
    {
        return ['channel-rw'];
    }

    public function broadcastAs()
    {
        return 'rw-event';
    }
}
