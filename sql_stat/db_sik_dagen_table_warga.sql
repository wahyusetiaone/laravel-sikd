create table table_warga
(
    _id            int auto_increment
        primary key,
    nik            varchar(16)  null,
    kk             varchar(16)  null,
    name           varchar(50)  null,
    place_of_birth varchar(50)  null,
    birthday       date         null,
    address        varchar(100) null,
    religion       varchar(25)  null,
    rt             varchar(3)   null,
    rw             varchar(3)   null,
    gender         varchar(15)  null,
    job_status     varchar(50)  null
)
    comment 'this for about identity peoples in the village';

INSERT INTO db_sik_dagen.table_warga (_id, nik, kk, name, place_of_birth, birthday, address, religion, rt, rw, gender, job_status) VALUES (1, '1234567891234567', '1234567891234500', 'Abah Setiawan', null, '2020-07-24', 'Dangen, 022/21, Karanganyar', null, '2', '7', 'Laki-laki', null);
INSERT INTO db_sik_dagen.table_warga (_id, nik, kk, name, place_of_birth, birthday, address, religion, rt, rw, gender, job_status) VALUES (2, '1234567897654321', '1234567897654300', 'Milea', null, '2020-07-15', 'Dangen, 21/21, Karanganyar', null, '2', null, 'Perempuan', null);
INSERT INTO db_sik_dagen.table_warga (_id, nik, kk, name, place_of_birth, birthday, address, religion, rt, rw, gender, job_status) VALUES (3, '31', '21', 'Ani', null, '2020-07-20', 'ffew', null, '1', null, 'Laki-laki', null);
INSERT INTO db_sik_dagen.table_warga (_id, nik, kk, name, place_of_birth, birthday, address, religion, rt, rw, gender, job_status) VALUES (4, '23432432432', '234322232323', 'Dea Nur Aviantika', null, '2020-07-17', 'Shdd 12,sosn,asda', null, '2', null, 'Perempuan', null);
INSERT INTO db_sik_dagen.table_warga (_id, nik, kk, name, place_of_birth, birthday, address, religion, rt, rw, gender, job_status) VALUES (5, '123450', '1234500', 'Mila Anjani', null, '1998-02-03', 'Tanjung, Dagen, Selo, Karanganyar', null, '6', null, 'Perempuan', null);
INSERT INTO db_sik_dagen.table_warga (_id, nik, kk, name, place_of_birth, birthday, address, religion, rt, rw, gender, job_status) VALUES (6, '100001', '110001', 'New Subject', null, '2020-11-04', 'Sukoharjo', null, '2', null, 'Laki-laki', null);
INSERT INTO db_sik_dagen.table_warga (_id, nik, kk, name, place_of_birth, birthday, address, religion, rt, rw, gender, job_status) VALUES (7, '1234567897654321', '1234567897654300', 'Milea', null, '2020-07-15', 'Dangen, 21/21, Karanganyar', null, '2', null, 'Perempuan', null);
INSERT INTO db_sik_dagen.table_warga (_id, nik, kk, name, place_of_birth, birthday, address, religion, rt, rw, gender, job_status) VALUES (9, '102121', '202121', 'UP', 'AOS', '2021-01-06', 'UP', 'ASD', '2', '2', 'Laki-laki', 'ASSS');