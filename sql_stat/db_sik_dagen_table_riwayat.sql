create table table_riwayat
(
    _id            int      not null,
    kode_transaksi int      null,
    dikeluarkan    datetime null,
    qr             text     null,
    release_number int      null,
    constraint table_riwayat__id_uindex
        unique (_id),
    constraint table_riwayat_table_transaksi__id_fk
        foreign key (kode_transaksi) references table_transaksi (_id)
)
    charset = latin1;

alter table table_riwayat
    add primary key (_id);

INSERT INTO db_sik_dagen.table_riwayat (_id, kode_transaksi, dikeluarkan, qr, release_number) VALUES (1, 10005, '2020-07-28 21:47:01', '"null"', 1);
INSERT INTO db_sik_dagen.table_riwayat (_id, kode_transaksi, dikeluarkan, qr, release_number) VALUES (2, 10002, '2020-07-29 00:58:45', 'null', 1);
INSERT INTO db_sik_dagen.table_riwayat (_id, kode_transaksi, dikeluarkan, qr, release_number) VALUES (3, 10009, '2021-01-11 00:00:00', 'dgfgfdg', 2);
INSERT INTO db_sik_dagen.table_riwayat (_id, kode_transaksi, dikeluarkan, qr, release_number) VALUES (4, 10001, '2021-01-28 18:52:14', 'null', 1);