create table table_rw
(
    _id  int auto_increment
        primary key,
    nip  varchar(20) null,
    nik  varchar(16) null,
    code varchar(3)  null
);

INSERT INTO db_sik_dagen.table_rw (_id, nip, nik, code) VALUES (1, '331201', '123450', '02');
INSERT INTO db_sik_dagen.table_rw (_id, nip, nik, code) VALUES (2, '23222', '100001', '7');