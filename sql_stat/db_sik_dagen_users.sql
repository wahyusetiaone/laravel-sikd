create table users
(
    id                bigint unsigned auto_increment
        primary key,
    name              varchar(255) not null,
    email             varchar(255) not null,
    email_verified_at timestamp    null,
    password          varchar(255) not null,
    remember_token    varchar(100) null,
    created_at        timestamp    null,
    updated_at        timestamp    null,
    constraint users_email_unique
        unique (email)
)
    collate = utf8mb4_unicode_ci;

INSERT INTO db_sik_dagen.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at) VALUES (1, 'Wahyu Setiawan', 'admin@material.com', '2020-07-28 06:32:02', '$2y$10$/FPF5Sh73fkYvYVDiG2LQOAu4i4GDh9PNEdqgKlSe4gzY.G1n230C', 'GWUPULGvDIoDjw2P9Mr54sZHfDq83MDRwP6OIH6CQ8WFyBHcE4Stl1HhwKHY', '2020-07-28 06:32:02', '2020-07-29 01:30:40');
INSERT INTO db_sik_dagen.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at) VALUES (2, 'Wahyu Setiawan', 'admin@sik.so', null, '$2y$10$1sqneHlQbaS4IjZBeS21vuHBZN8f75B8D1DXf7wJcir6TXZyOuz1.', null, '2020-07-29 01:32:46', '2020-07-29 01:32:46');