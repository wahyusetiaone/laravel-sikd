create table table_transaksi
(
    _id          int         not null
        primary key,
    nik          varchar(16) null,
    kode_surat   int         null,
    created_date date        null,
    keperluan    text        null,
    hsv          int         null,
    status       varchar(2)  null,
    constraint table_transaksi_table_surat__id_fk
        foreign key (kode_surat) references table_surat (_id)
)
    charset = latin1;

INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10001, '1234567891234567', 9001, '2019-01-09', 'Untuk Melamar Kerja', 13, '1');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10002, '1234567891234567', 9002, '2019-01-02', 'Acara Nikahan', null, '1');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10003, '23432432432', 9001, '2020-01-16', 'Untuk Biasa', null, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10004, '23432432432', 9002, '2020-01-15', 'Acara Tawuran', null, 'K0');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10005, '1234567897654321', 9004, '2020-01-16', 'Membuat Surat Kematian atas nama someone', null, '1');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10006, '31', 9001, '2020-01-17', 'A', null, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10007, '123450', 9001, '2020-01-22', 'ase', null, 'R0');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10008, '123450', 9004, '2020-01-15', 'Aes', null, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10009, '123450', 9002, '2020-01-30', 'Saya', null, '1');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10010, '31', 9001, '2021-01-27', 'Coba HSV', 5, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10011, '102121', 9001, '2021-01-27', 'V2new', 6, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10012, '102121', 9001, '2021-01-27', 'mmmmnmmmm', 7, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10013, '31', 9001, '2021-01-27', 'Coba HSV 1', 8, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10014, '102121', 9001, '2021-01-27', 'bbbb', 9, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10015, '102121', 9001, '2021-01-27', 'ket', 10, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10016, '102121', 9006, '2021-01-28', 'Jenis Pembuatan KTP : Baru', 11, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10017, '102121', 9005, '2021-01-28', null, 12, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10018, '102121', 9001, '2021-01-28', 'Pembuatan SKTM untuk beasiswa', 13, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10019, '102121', 9002, '2021-01-28', 'Pembuatan SIH untuk Nikah Anak', 14, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10020, '102121', 9003, '2021-01-28', 'Pembuatan SKK', 15, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10021, '102121', 9004, '2021-01-28', 'Pembuatan SKDU PT.XYZ', 16, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10022, '102121', 9005, '2021-01-28', 'Kelahiran A.N Melia', 17, 'R');
INSERT INTO db_sik_dagen.table_transaksi (_id, nik, kode_surat, created_date, keperluan, hsv, status) VALUES (10023, '102121', 9006, '2021-01-28', 'Jenis Pembuatan KTP : Baru', 18, 'R');