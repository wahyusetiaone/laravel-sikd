create table table_surat
(
    _id        int         not null,
    nama_surat varchar(50) null,
    format     varchar(50) null,
    lasted     int         null,
    constraint table_surat__id_uindex
        unique (_id)
)
    charset = latin1;

alter table table_surat
    add primary key (_id);

INSERT INTO db_sik_dagen.table_surat (_id, nama_surat, format, lasted) VALUES (9001, 'Surat Keterangan Tidak Mampu', '021/X/VII/2021', 1);
INSERT INTO db_sik_dagen.table_surat (_id, nama_surat, format, lasted) VALUES (9002, 'Surat Izin Hiburan', '022/X/VII/2021', 2);
INSERT INTO db_sik_dagen.table_surat (_id, nama_surat, format, lasted) VALUES (9003, 'Surat Keterangan Kematian', '023/X/VII/2021', 0);
INSERT INTO db_sik_dagen.table_surat (_id, nama_surat, format, lasted) VALUES (9004, 'Surat Keterangan Domisili Usaha', '024/X/VII/2021', 0);
INSERT INTO db_sik_dagen.table_surat (_id, nama_surat, format, lasted) VALUES (9005, 'Surat Keterangan Kelahiran', '025/X/VII/2021', 0);
INSERT INTO db_sik_dagen.table_surat (_id, nama_surat, format, lasted) VALUES (9006, 'Surat Pengantar KTP', '026/X/VII/2021', 0);