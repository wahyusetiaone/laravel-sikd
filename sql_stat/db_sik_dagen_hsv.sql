create table hsv
(
    _id int  not null
        primary key,
    val text null
);

INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (1, 'satu.#dua.#tiga.#empat.#lima.');
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (2, 'ini#adalah#string');
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (3, 'ini#adalah#string');
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (4, 'ini#adalah#string');
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (5, 'ini#adalah#string#hsv');
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (6, null);
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (7, null);
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (8, 'ini#adalah#string#hsv jkjk mmm');
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (9, null);
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (10, 'auah#ibu#untuk selolah A');
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (11, 'Jenis Pembuatan KTP : Baru');
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (12, '###############');
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (13, 'Paidi#Misiyem#untuk mendapatkan beasiswa di kampus A');
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (14, 'Selasa, 26 Januari 2021#Dangdut Koplo#750 Orang#Nikahan anak tersayang');
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (15, '01 Januari 1999#13:40 WIB#Sakit#RS Mangunnan#Dokter Paijo#Larno#Sulis#Sudjman#Agus#Kotak');
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (16, 'PT.XYZ#Dagen, Karanganyar');
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (17, 'Agus#3312837719380005#Melia Devana#Perempuan#Karanganyar#RS Soedjman#Selasa,02 Januari 2021#09:30#Sesar#Dokter Ajik#3,6 Kg#47 cm#Enny#Agus#Satpam#Suster');
INSERT INTO db_sik_dagen.hsv (_id, val) VALUES (18, 'Jenis Pembuatan KTP : Baru');