create table table_rt
(
    _id  int auto_increment
        primary key,
    nip  varchar(20) null,
    nik  varchar(16) null,
    code varchar(3)  null
)
    comment 'for rt identity';

INSERT INTO db_sik_dagen.table_rt (_id, nip, nik, code) VALUES (1, '123', '1234567891234567', '2');