<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/rt/login', 'Rest\ApiRtController@login');
Route::post('/rt/get', 'Rest\ApiRtController@get');
Route::post('/rt/history', 'Rest\ApiRtController@history');
Route::post('/rt/historyWaiting', 'Rest\ApiRtController@historyWaiting');
Route::post('/rt/update', 'Rest\ApiRtController@update');

Route::post('/rw/login', 'Rest\ApiRwController@login');
Route::post('/rw/get', 'Rest\ApiRwController@get');
Route::post('/rw/history', 'Rest\ApiRwController@history');
Route::post('/rw/historyWaiting', 'Rest\ApiRwController@historyWaiting');
Route::post('/rw/update', 'Rest\ApiRwController@update');

Route::post('/warga/login', 'Rest\ApiWargaController@login');
Route::post('/warga/get', 'Rest\ApiWargaController@get');
Route::post('/warga/history', 'Rest\ApiWargaController@history');
Route::post('/warga/request', 'Rest\ApiWargaController@request');
Route::post('/warga/requestv2', 'Rest\ApiWargaController@requestv2');
