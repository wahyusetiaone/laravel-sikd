<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Events\SendGlobalNotificationRt;
use App\Events\TestEvent;
use Pusher\Pusher;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/broadcast', function() {
    $pusher = new Pusher( "3333c813add5c2d7360f", "e9e05f75aa3d5f815399", "1155280", array( 'cluster' => "ap1", 'useTLS' => true ) );

    $data = "rt".","."rw".","."nik".","."Wahyu Setiawan".","."kode_surat".","."Surat Keterangan Tidak Mampu".","."id_transaksi";

    $pusher->trigger( 'channel-rt', 'rt-event', $data );
    return view('welcome');
});

Auth::routes();

Route::get('/logs', 'SuratController@log')->name('logs');

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');

Route::get('surat_sktm/{no}', 'SuratController@sktm')->name("sktm");
Route::get('surat_sip/{no}', 'SuratController@sip')->name("sip");
Route::get('surat_skdu/{no}', 'SuratController@skdu')->name("skdu");
Route::get('surat_skk/{no}', 'SuratController@skk')->name("skk");
Route::get('surat_skl/{no}', 'SuratController@skl')->name("skl");
Route::get('surat_skktp/{no}', 'SuratController@skktp')->name("skktp");

Route::get('p/laporan', 'PrintLaporanController@index')->name("print_report_all");
Route::get('p/laporan/today', 'PrintLaporanController@today')->name("print_report_today");
Route::get('p/laporan/oneweek', 'PrintLaporanController@oneweek')->name("print_report_oneweek");

Route::group(['middleware' => 'auth'], function () {

    Route::post('session_set', 'SessionController@set')->name("session_set");
    Route::post('session_get', 'SessionController@get')->name("session_get");
    Route::post('session_forget', 'SessionController@forget')->name("session_forget");


    Route::get('citizen-list', 'CitizenController@index')->name("warga");
    Route::get('citizen-form', 'CitizenController@form')->name("warga.form");
    Route::post('citizen-add', 'CitizenController@add')->name("warga.add");
    Route::get('citizen-show/{nik}', 'CitizenController@show')->name("warga.show");
    Route::post('citizen-update', 'CitizenController@update')->name("warga.update");
    Route::get('citizen-delete/{nik}', 'CitizenController@delete')->name("warga.delete");

    Route::get('rt-list', 'RTController@index')->name("rt");
    Route::get('rt-form', 'RTController@form')->name("rt.form");
    Route::get('rt-find/{nik}', 'RTController@find')->name("rt.find");
    Route::post('rt-add', 'RTController@add')->name("rt.add");
    Route::get('rt-delete/{nip}', 'RTController@delete')->name("rt.delete");

    Route::get('rw-list', 'RWController@index')->name("rw");
    Route::get('rw-form', 'RWController@form')->name("rw.form");
    Route::get('rw-find/{nik}', 'RWController@find')->name("rw.find");
    Route::post('rw-add', 'RWController@add')->name("rw.add");
    Route::get('rw-delete/{nip}', 'RWController@delete')->name("rw.delete");

    Route::get('history-list', 'HistoryController@index')->name("history");
    Route::get('history-confirm-letter/{i}/{k}/{st}/{ks}/{ns}/{nik}/{kk}/{n}/{b}/{a}/{r}/{g}', 'HistoryController@confirm')->name("history.confirm");
    Route::get('history-detail-letter/{i}/{k}/{st}/{ks}/{ns}/{nik}/{kk}/{n}/{b}/{a}/{r}/{g}/{d}/{q}/{f}/{no}', 'HistoryController@details')->name("history.details");
    Route::post('history-update', 'HistoryController@update')->name("history.update");

    Route::get('report-list', 'ReportController@index')->name("report");
    Route::get('report-list/filter-today', 'ReportController@today')->name("report.filter.today");
    Route::get('report-list/filter-oneweek', 'ReportController@oneweek')->name("report.filter.oneweek");
    Route::get('report-detail-letter/{i}/{k}/{st}/{ks}/{ns}/{nik}/{kk}/{n}/{b}/{a}/{r}/{g}/{d}/{q}/{f}/{no}', 'ReportController@details')->name("report.details");

    Route::get('table-list', function () {
        return view('pages.table_list');
    })->name('table');

    Route::get('typography', function () {
        return view('pages.typography');
    })->name('typography');

    Route::get('icons', function () {
        return view('pages.icons');
    })->name('icons');

    Route::get('map', function () {
        return view('pages.map');
    })->name('map');

    Route::get('notifications', function () {
        return view('pages.notifications');
    })->name('notifications');

    Route::get('rtl-support', function () {
        return view('pages.language');
    })->name('language');

    Route::get('upgrade', function () {
        return view('pages.upgrade');
    })->name('upgrade');
});

Route::group(['middleware' => 'auth'], function () {
    Route::resource('user', 'UserController', ['except' => ['show']]);
    Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
    Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

