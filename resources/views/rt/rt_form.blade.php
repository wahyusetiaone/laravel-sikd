@extends('layouts.app', ['activePage' => 'rt', 'titlePage' => __('Add a Citizen')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('rt.add') }}" autocomplete="off"
                          class="form-horizontal">
                        @csrf
                        @method('post')

                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Form Profil RT') }}</h4>
                                <p class="card-category">{{ __('Informasi RT') }}</p>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('NIP') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('nip') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('nip') ? ' is-invalid' : '' }}"
                                                   name="nip" id="input-nip" type="number"
                                                   placeholder="{{ __('NIP') }}"
                                                   required="true"
                                                   aria-required="true"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('NIK') }}</label>
                                    <div class="col-sm-5">
                                        <div class="form-group{{ $errors->has('nik') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('nik') ? ' is-invalid' : '' }}"
                                                   name="nik" id="input-nik" type="number"
                                                   placeholder="{{ __('NIK') }}"
                                                   required="true"
                                                   @if(session()->has('nik_for_rt'))
                                                   value="{{session()->get('nik_for_rt')}}"
                                                   @endif
                                                   readonly
                                                   aria-required="true"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <a href="{{route('rt.find',[0])}}">
                                        <button type="button" class="btn btn-primary">
                                            Find
                                        </button>
                                        </a>
                                    </div>
                                </div>
{{--                                <div class="row">--}}
{{--                                    <label class="col-sm-2 col-form-label">{{ __('RT') }}</label>--}}
{{--                                    <div class="col-sm-7">--}}
{{--                                        <div class="form-group{{ $errors->has('code') ? ' has-danger' : '' }}">--}}
{{--                                            <input class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}"--}}
{{--                                                   name="code" id="input-code" type="number"--}}
{{--                                                   placeholder="{{ __('RT') }}"--}}
{{--                                                   required="true"--}}
{{--                                                   aria-required="true"/>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" onclick="forgetTheKey('nik_for_rt')" class="btn btn-primary">{{ __('Save') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
