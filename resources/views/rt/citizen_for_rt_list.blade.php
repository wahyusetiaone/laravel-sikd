@extends('layouts.app', ['activePage' => 'rt', 'titlePage' => __('Table List of Citizen')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">NIK</h4>
                            <div class="row">
                                <div class="col-sm-4">
                                    <input class="form-control{{ $errors->has('nik') ? ' is-invalid' : '' }}"
                                           name="nik" id="input-nik" type="number"
                                           placeholder="{{ __('NIK') }}"
                                           required="true"
                                           aria-required="true"/>
                                </div>
                                <div class="col-sm-2">
                                    <button type="button" onclick="findNik();" id="btn_find_nik" class="btn btn-primary">
                                        Cari NIK
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        NIK
                                    </th>
                                    <th>
                                        KK
                                    </th>
                                    <th>
                                        NAME
                                    </th>
                                    <th>
                                        ADDRESS
                                    </th>
                                    <th>
                                        GENRE
                                    </th>
                                    <th>

                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $value)
                                        <tr>
                                            <td>
                                                {{$value->_id}}
                                            </td>
                                            <td>
                                                {{$value->nik}}
                                            </td>
                                            <td>
                                                {{$value->kk}}
                                            </td>
                                            <td>
                                                {{$value->name}}
                                            </td>
                                            <td class="text-primary">
                                                {{$value->address}}
                                            </td>
                                            <td>
                                                {{$value->gender}}
                                            </td>
                                            <td>
                                                <button type="button" onclick="setNikRt({{$value->nik}});" id="btn_set_nik" class="btn btn-primary">
                                                    SET
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
