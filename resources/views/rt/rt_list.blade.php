@extends('layouts.app', ['activePage' => 'rt', 'titlePage' => __('Table List of RT')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Tabel RT</h4>
                            <p class="card-category"> Semua Data RT !!!</p>
                            <a href="{{route('rt.form')}}">
                                <button class="btn btn-primary"
                                        style="position: absolute; top: 0; right: 0; margin: 20px; display: inline-block;">
                                    Tambah RT
                                </button>
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        SK
                                    </th>
                                    <th>
                                        RT
                                    </th>
                                    <th>
                                        RW
                                    </th>
                                    <th>
                                        NAME
                                    </th>
                                    <th>
                                        ADDRESS
                                    </th>
                                    <th>
                                        GENRE
                                    </th>
                                    <th>
                                        ACTION
                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $value)
                                        <tr>
                                            <td>
                                                {{$value->_id}}
                                            </td>
                                            <td>
                                                {{$value->nip}}
                                            </td>
                                            <td>
                                                {{$value->code}}
                                            </td>
                                            <td>
                                                {{$value->rw}}
                                            </td>
                                            <td>
                                                {{$value->name}}
                                            </td>
                                            <td>
                                                {{$value->address}}
                                            </td>
                                            <td>
                                                {{$value->gender}}
                                            </td>
                                            <td>
                                                <a href="{{route('rt.delete',['nip'=>$value->nip])}}">
                                                    <button type="button" class="btn btn-danger">{{ __('Delete') }}</button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
