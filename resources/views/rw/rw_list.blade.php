@extends('layouts.app', ['activePage' => 'rw', 'titlePage' => __('Table List of RW')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Tabel RW</h4>
                            <p class="card-category"> Data Semua RW !!!</p>
                            <a href="{{route('rw.form')}}">
                                <button class="btn btn-primary"
                                        style="position: absolute; top: 0; right: 0; margin: 20px; display: inline-block;">
                                    Tambah RW
                                </button>
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        SK
                                    </th>
                                    <th>
                                        RW
                                    </th>
                                    <th>
                                        NAME
                                    </th>
                                    <th>
                                        ADDRESS
                                    </th>
                                    <th>
                                        GENRE
                                    </th>
                                    <th>
                                        ACTION
                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $value)
                                        <tr>
                                            <td>
                                                {{$value->_id}}
                                            </td>
                                            <td>
                                                {{$value->nip}}
                                            </td>
                                            <td>
                                                {{$value->code}}
                                            </td>
                                            <td>
                                                {{$value->name}}
                                            </td>
                                            <td class="text-primary">
                                                {{$value->address}}
                                            </td>
                                            <td>
                                                {{$value->gender}}
                                            </td>
                                            <td>
                                                <a href="{{route('rw.delete',['nip'=>$value->nip])}}">
                                                    <button type="button" class="btn btn-danger">{{ __('Delete') }}</button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
