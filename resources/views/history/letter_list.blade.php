@extends('layouts.app', ['activePage' => 'history', 'titlePage' => __('Table List of Letter')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="row">
                                <div class="col-md-10">
                                    @if($page == "all")
                                    <h4 class="card-title ">Daftar Surat Pengajuan</h4>
                                    <p class="card-category"> Seluruh surat yang diajukan oleh warga !!!</p>
                                    @endif
                                </div>

                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        Nama Pengaju
                                    </th>
                                    <th>
                                        Jenis Surat
                                    </th>
                                    <th>
                                        Tgl Pengajuan
                                    </th>
                                    <th>
                                        Nomor Surat
                                    </th>
                                    <th>
                                        Keperluan
                                    </th>
                                    <th></th>
                                    <th>Action</th>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $value)
                                        <tr>
                                            <td>
                                                {{$value->_id_transaksi - 10000}}
                                            </td>
                                            <td>
                                                {{$value->name}}
                                            </td>
                                            <td>
                                                {{$value->nama_surat}}
                                            </td>
                                            <td>
                                                @php
                                                    echo App\Http\Controllers\ReportController::tgl_indo($value->tgl_pengajuan);
                                                @endphp
                                            </td>
                                            <td>
                                                @if($value->status == 1)
                                                    <?php
                                                    $no = str_replace('X', $value->no_surat, $value->format);
                                                    print $no;
                                                    ?>
                                                @else
                                                    Tidak Ada
                                                @endif
                                            </td>
                                            <td>
                                                @if(strlen($value->keperluan) <=25)
                                                    {{$value->keperluan}}
                                                @else
                                                    {{substr($value->keperluan, 0, 25) . '...'}}
                                                @endif
                                            </td>
                                            @if($value->status == 1)
                                                <td class="text-success">
                                                    Disetujui
                                                </td>
                                                <td>
                                                    @if(empty($value->no_surat))
                                                        @php($no_surat = 0)
                                                    @else
                                                        @php($no_surat = $value->no_surat)
                                                    @endif
                                                        <a href="{{route('history.details', [$value->_id_transaksi, $value->keperluan, $value->status, $value->kode_surat, $value->nama_surat, $value->nik, $value->kk, $value->name, $value->birthday, str_replace('/', ' ', $value->address), $value->rt, $value->gender, date('Y-m-d', strtotime($value->dikeluarkan)), 'NOT', str_replace('/', '-', $value->format), $no_surat])}}">
                                                            <button class="btn btn-info">
                                                                Lihat
                                                            </button>
                                                        </a>
                                                </td>
                                            @elseif($value->status == 'K0')
                                                <td class="text-danger">
                                                    Ditolak
                                                </td>
                                                <td></td>
                                            @elseif($value->status == 'K')
                                                <td class="text-warning">
                                                    Menunggu
                                                </td>
                                                <td>
                                                    <a href="{{route('history.confirm', [$value->_id_transaksi, $value->keperluan, $value->status, $value->kode_surat, $value->nama_surat, $value->nik, $value->kk, $value->name, $value->birthday, str_replace('/', '-', $value->address), $value->rt, $value->gender])}}">
                                                        <button class="btn btn-warning">
                                                            Konfirmasi
                                                        </button>
                                                    </a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

