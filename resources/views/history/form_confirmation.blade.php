@extends('layouts.app', ['activePage' => 'history', 'titlePage' => __('Confirmation Form')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ __('Halaman Konfirmasi Surat') }}</h4>
                            <p class="card-category">{{ __('Baca dengan teliti sebelum melakukan konfirmasi surat !') }}</p>
                        </div>
                        <div class="card-body ">
                            <div class="row">
                                <label class="col-sm-2 font-weight-bold col-form-label">{{ __('NIK') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(7)}}</label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 font-weight-bold col-form-label">{{ __('KK') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(8)}}</label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 font-weight-bold col-form-label">{{ __('NAMA') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(9)}}</label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 font-weight-bold col-form-label">{{ __('ALAMAT') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(11)}}</label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 font-weight-bold col-form-label">{{ __('JENIS KELAMIN') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(13)}}</label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 font-weight-bold col-form-label">{{ __('JENIS SURAT') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(6)}}</label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 font-weight-bold col-form-label">{{ __('KEPERLUAN') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(3)}}</label>
                            </div>
                        </div>
                        <div class="card-footer ml-auto mr-auto">
                           <div class="row">
                               <div class="col-sm-4 ml-auto mr-auto">
                                   <button type="button" onclick="terimaSurat({{request()->segment(2)}});"
                                           class="btn btn-success">{{ __('Approved') }}</button>
                               </div>
                               <div class="col-sm-4 ml-auto mr-auto">
                                   <button type="button" onclick="tolakSurat({{request()->segment(2)}});"
                                           class="btn btn-danger">{{ __('Rejected') }}</button>
                               </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
