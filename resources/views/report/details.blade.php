@extends('layouts.app', ['activePage' => 'history', 'titlePage' => __('Details Letter')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="card-header card-header-primary">
                           <?php
                            $str = str_replace('-', '/', request()->segment(16));
                            $no = str_replace('X', request()->segment(17), $str);
                            ?>
                            <h4 class="card-title">{{ __('Latter of ').$no}}</h4>
                            <p class="card-category">{{ __('Letter information detail') }}</p>
                        </div>
                        <div class="card-body ">
                            <div class="row">
                                <label class="col-sm-2 font-weight-bold col-form-label">{{ __('NIK') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(7)}}</label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 font-weight-bold col-form-label">{{ __('KK') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(8)}}</label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 font-weight-bold col-form-label">{{ __('NAMA') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(9)}}</label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 font-weight-bold col-form-label">{{ __('ALAMAT') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(11)}}</label>
                            </div>
                            <div class="row">
                                <label
                                    class="col-sm-2 font-weight-bold col-form-label">{{ __('JENIS KELAMIN') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(13)}}</label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 font-weight-bold col-form-label">{{ __('JENIS SURAT') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(6)}}</label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 font-weight-bold col-form-label">{{ __('KEPERLUAN') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(3)}}</label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 font-weight-bold col-form-label">{{ __('DIKELUARKAN') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(14)}}</label>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 font-weight-bold col-form-label">{{ __('QR') }}</label>
                                <label class="col-sm-10 col-form-label">: {{request()->segment(15)}}</label>
                            </div>
                        </div>
                        <div class="card-footer ml-auto mr-auto">
                            <div class="row">
                                <div class="col-sm-4 ml-auto mr-auto">
                                    <a href="{{route('report')}}">
                                        <button type="button" class="btn btn-secondary">{{ __('Back') }}</button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
