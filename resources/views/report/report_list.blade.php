@extends('layouts.app', ['activePage' => 'report', 'titlePage' => __('Table List of Letter')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <div class="row">
                                <div class="col-md-10">
                                    @if($page == "all")
                                        <h4 class="card-title ">Daftar Surat diterbitkan</h4>
                                        <p class="card-category"> Seluruh surat yang telah diterbitkan !!!</p>

                                        <button class="btn btn-info" onclick="btn_cetak_all()">
                                            Cetak Laporan
                                        </button>
                                    @elseif($page == "today")
                                        <h4 class="card-title ">Daftar Surat Terbit Hari ini</h4>
                                        <p class="card-category"> Daftar Surat hari ini tanggal {{$tgl}} !!!</p>

                                        <button class="btn btn-info" onclick="btn_cetak_today()">
                                            Cetak Laporan
                                        </button>
                                    @elseif($page == "oneweek" )
                                        <h4 class="card-title ">Daftar Surat Terbit 7 hari terakhir</h4>
                                        <p class="card-category"> Daftar Surat mulai tanggal {{$tglawal}}
                                            sampai {{$tglakhir}} !!!</p>

                                        <button class="btn btn-info" onclick="btn_cetak_oneweek()">
                                            Cetak Laporan
                                        </button>
                                    @endif
                                </div>
                                <div class="col-md-2" style="text-align: center">
                                    <h6>Filter Surat</h6>
                                    @if($page == "all")
                                        <button class="btn btn-outline-light" onclick="btn_filter_all()">
                                            Semua
                                        </button>
                                    @elseif($page == "today")
                                        <button class="btn btn-outline-light" onclick="btn_filter_today()">
                                            Hari ini
                                        </button>
                                    @elseif($page == "oneweek" )
                                        <button class="btn btn-outline-light" onclick="btn_filter_oneweek()">
                                            7 Hari
                                        </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        Nama Pengaju
                                    </th>
                                    <th>
                                        Jenis Surat
                                    </th>
                                    <th>
                                        Tgl Pengajuan
                                    </th>
                                    <th>
                                        Nomor Surat
                                    </th>
                                    <th>
                                        Keperluan
                                    </th>
                                    <th></th>
{{--                                    <th>Action</th>--}}
                                    </thead>
                                    <tbody>
                                    @foreach($data as $value)
                                        <tr>
                                            <td>
                                                {{$value->_id_transaksi - 10000}}
                                            </td>
                                            <td>
                                                {{$value->name}}
                                            </td>
                                            <td>
                                                {{$value->nama_surat}}
                                            </td>
                                            <td>
                                                @php
                                                    echo App\Http\Controllers\ReportController::tgl_indo($value->tgl_pengajuan);
                                                @endphp
                                            </td>
                                            <td>
                                                @if($value->status == 1)
                                                    <?php
                                                    $no = str_replace('X', $value->no_surat, $value->format);
                                                    print $no;
                                                    ?>
                                                @else
                                                    Tidak Ada
                                                @endif
                                            </td>
                                            <td>
                                                @if(strlen($value->keperluan) <=25)
                                                    {{$value->keperluan}}
                                                @else
                                                    {{substr($value->keperluan, 0, 25) . '...'}}
                                                @endif
                                            </td>
                                            @if($value->status == 1)
                                                <td class="text-success">
                                                    Disetujui
                                                </td>
{{--                                                <td>--}}
{{--                                                    <a href="{{route('report.details', [$value->_id_transaksi, $value->keperluan, $value->status, $value->kode_surat, $value->nama_surat, $value->nik, $value->kk, $value->name, $value->birthday, str_replace('/', '-', $value->address), $value->rt, $value->gender, $value->dikeluarkan, $value->qr, str_replace('/', '-', $value->format), $value->no_surat])}}">--}}
{{--                                                        <button class="btn btn-info">--}}
{{--                                                            Lihat--}}
{{--                                                        </button>--}}
{{--                                                    </a>--}}
{{--                                                </td>--}}
                                            @elseif($value->status == 'K0')
                                                <td class="text-danger">
                                                    Ditolak
                                                </td>
                                                <td></td>
                                            @elseif($value->status == 'K')
                                                <td class="text-warning">
                                                    Menunggu
                                                </td>
                                                <td>
                                                    <a href="{{route('history.confirm', [$value->_id_transaksi, $value->keperluan, $value->status, $value->kode_surat, $value->nama_surat, $value->nik, $value->kk, $value->name, $value->birthday, str_replace('/', '-', $value->address), $value->rt, $value->gender])}}">
                                                        <button class="btn btn-warning">
                                                            Konfirmasi
                                                        </button>
                                                    </a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection

