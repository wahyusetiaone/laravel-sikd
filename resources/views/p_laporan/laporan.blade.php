<!DOCTYPE html>
<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td, th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>
<body>

@if($page == 'all')
    <h2>Laporan Keseluruhan Surat Keluar</h2>
    <h3>Kelurahan Dagen, Karanganyar</h3>
@elseif($page == 'today')
    <h2>Laporan Keseluruhan Surat Keluar Pada Tanggal {{$tgl}}</h2>
    <h3>Kelurahan Dagen, Karanganyar</h3>
@elseif($page == 'oneweek')
    <h2>Laporan Keseluruhan Surat Keluar Periode Tanggal {{$tglawal}} - {{$tglakhir}}</h2>
    <h3>Kelurahan Dagen, Karanganyar</h3>
@endif

@php
    $nomor_sktm = 0;
    $nomor_sip = 0;
    $nomor_skdu = 0;
    $nomor_skk = 0;
    $nomor_skl = 0;
    $nomor_skktp = 0;
@endphp

<div>
    <table>
        <tr>
            <th style="text-align: center">Surat Keterangan Tidak Mampu</th>
        </tr>
    </table>
    <table>
        @if(empty($sktm))
            <tr style="text-align: center"> Tidak ada data</tr>
        @else
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Keperluan</th>
                <th>Nomor Surat</th>
                <th>Tgl Terbit</th>
            </tr>
            @foreach($sktm as $e)
                <tr>
                    <td>{{$nomor_sktm += 1}}</td>
                    <td>{{$e->name}}</td>
                    <td>{{$e->address}}</td>
                    <td>{{$e->keperluan}}</td>
                    <td>{{$e->num}}/{{$e->format}} </td>
                    <td>
                        @php
                            $rec = $e->dikeluarkan;
                            $tgl = new DateTime($rec);
                            $strip = $tgl->format('Y-m-d');
                            echo App\Http\Controllers\PrintLaporanController::tgl_indo($strip);
                        @endphp
                    </td>
                </tr>
            @endforeach
        @endif
    </table>
</div>
<br>
<div>
    <table>
        <tr>
            <th style="text-align: center">Surat Izin Perjamuan</th>
        </tr>
    </table>
    <table>
        @if(empty($sip))
            <tr style="text-align: center"> Tidak ada data</tr>
        @else
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Keperluan</th>
                <th>Nomor Surat</th>
                <th>Tgl Terbit</th>
            </tr>
            @foreach($sip as $e)
                <tr>
                    <td>{{$nomor_sip += 1}}</td>
                    <td>{{$e->name}}</td>
                    <td>{{$e->address}}</td>
                    <td>{{$e->keperluan}}</td>
                    <td>{{$e->num}}/{{$e->format}} </td>
                    <td>
                        @php
                            $rec = $e->dikeluarkan;
                            $tgl = new DateTime($rec);
                            $strip = $tgl->format('Y-m-d');
                            echo App\Http\Controllers\PrintLaporanController::tgl_indo($strip);
                        @endphp
                    </td>
                </tr>
            @endforeach
        @endif
    </table>
</div>
<br>
<div>
    <table>
        <tr>
            <th style="text-align: center">Surat Keterangan Domisili Usaha</th>
        </tr>
    </table>
    <table>
        @if(empty($sip))
            <tr style="text-align: center"> Tidak ada data</tr>
        @else
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Keperluan</th>
                <th>Nomor Surat</th>
                <th>Tgl Terbit</th>
            </tr>
            @foreach($skdu as $e)
                <tr>
                    <td>{{$nomor_skdu += 1}}</td>
                    <td>{{$e->name}}</td>
                    <td>{{$e->address}}</td>
                    <td>{{$e->keperluan}}</td>
                    <td>{{$e->num}}/{{$e->format}} </td>
                    <td>
                        @php
                            $rec = $e->dikeluarkan;
                            $tgl = new DateTime($rec);
                            $strip = $tgl->format('Y-m-d');
                            echo App\Http\Controllers\PrintLaporanController::tgl_indo($strip);
                        @endphp
                    </td>
                </tr>
            @endforeach
        @endif
    </table>
</div>
<br>
<div>
    <table>
        <tr>
            <th style="text-align: center">Surat Keterangan Kematian</th>
        </tr>
    </table>
    <table>
        @if(empty($skk))
            <tr style="text-align: center"> Tidak ada data</tr>
        @else
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Keperluan</th>
                <th>Nomor Surat</th>
                <th>Tgl Terbit</th>
            </tr>
            @foreach($skk as $e)
                <tr>
                    <td>{{$nomor_skk += 1}}</td>
                    <td>{{$e->name}}</td>
                    <td>{{$e->address}}</td>
                    <td>{{$e->keperluan}}</td>
                    <td>{{$e->num}}/{{$e->format}} </td>
                    <td>
                        @php
                            $rec = $e->dikeluarkan;
                            $tgl = new DateTime($rec);
                            $strip = $tgl->format('Y-m-d');
                            echo App\Http\Controllers\PrintLaporanController::tgl_indo($strip);
                        @endphp
                    </td>
                </tr>
            @endforeach
        @endif
    </table>
</div>
<br>
<div>
    <table>
        <tr>
            <th style="text-align: center">Surat Keterangan Kelahiran</th>
        </tr>
    </table>
    <table>
        @if(empty($skl))
            <tr style="text-align: center"> Tidak ada data</tr>
        @else
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Keperluan</th>
                <th>Nomor Surat</th>
                <th>Tgl Terbit</th>
            </tr>
            @foreach($skl as $e)
                <tr>
                    <td>{{$nomor_skl += 1}}</td>
                    <td>{{$e->name}}</td>
                    <td>{{$e->address}}</td>
                    <td>{{$e->keperluan}}</td>
                    <td>{{$e->num}}/{{$e->format}} </td>
                    <td>
                        @php
                            $rec = $e->dikeluarkan;
                            $tgl = new DateTime($rec);
                            $strip = $tgl->format('Y-m-d');
                            echo App\Http\Controllers\PrintLaporanController::tgl_indo($strip);
                        @endphp
                    </td>
                </tr>
            @endforeach
        @endif
    </table>
</div>
<br>
<div>
    <table>
        <tr>
            <th style="text-align: center">Surat Pengantar KTP</th>
        </tr>
    </table>
    <table>
        @if(empty($skktp))
            <tr style="text-align: center"> Tidak ada data</tr>
        @else
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>Keperluan</th>
                <th>Nomor Surat</th>
                <th>Tgl Terbit</th>
            </tr>
            @foreach($skktp as $e)
                <tr>
                    <td>{{$nomor_skktp += 1}}</td>
                    <td>{{$e->name}}</td>
                    <td>{{$e->address}}</td>
                    <td>{{$e->keperluan}}</td>
                    <td>{{$e->num}}/{{$e->format}} </td>
                    <td>
                        @php
                            $rec = $e->dikeluarkan;
                            $tgl = new DateTime($rec);
                            $strip = $tgl->format('Y-m-d');
                            echo App\Http\Controllers\PrintLaporanController::tgl_indo($strip);
                        @endphp
                    </td>
                </tr>
            @endforeach
        @endif
    </table>
</div>

<script>
    $(document).ready(function () {
        window.print();
    });
</script>
</body>
</html>
