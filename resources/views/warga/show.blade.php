@extends('layouts.app', ['activePage' => 'warga', 'titlePage' => __('Warga')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <form method="post" action="{{ route('warga.update') }}" autocomplete="off"
                              class="form-horizontal">
                            @csrf
                            @method('post')

                            <div class="card-header card-header-primary">
                            <h4 class="card-title">{{ __('NIK ')}} {{$data->nik}}</h4>
                            <p class="card-category">{{ __('Informasi detail') }}</p>
                        </div>
                        <div class="card-body ">
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('NIK') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('nik') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('nik') ? ' is-invalid' : '' }}"
                                               name="nik" id="input-nik" type="number"
                                               placeholder="{{ __('NIK') }}"
                                               required="true"
                                               value="{{$data->nik}}"
                                               readonly
                                               aria-required="true"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('KK') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('kk') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('kk') ? ' is-invalid' : '' }}"
                                               name="kk" id="input-kk" type="number"
                                               placeholder="{{ __('KK') }}"
                                               required="true"
                                               value="{{$data->kk}}"
                                               aria-required="true"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Nama') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                               name="name" id="input-name" type="text"
                                               placeholder="{{ __('Nama') }}"
                                               required="true"
                                               value="{{$data->name}}"
                                               aria-required="true"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Tempat Tanggal Lahir') }}</label>
                                <div class="col-sm-4">
                                    <div class="form-group{{ $errors->has('place_of_birth') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('place_of_birth') ? ' is-invalid' : '' }}"
                                               name="place_of_birth" id="input-place_of_birth" type="text"
                                               placeholder="{{ __('Tempat Lahir') }}"
                                               required="true"
                                               value="{{$data->place_of_birth}}"
                                               aria-required="true"/>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group{{ $errors->has('birthday') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('birthday') ? ' is-invalid' : '' }}"
                                               name="birthday" id="input-birthday" type="date"
                                               placeholder="{{ __('Tanggal') }}"
                                               required="true"
                                               value="{{$data->birthday}}"
                                               aria-required="true"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Alamat') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}"
                                               name="address" id="input-address" type="text"
                                               placeholder="{{ __('Alamat') }}"
                                               required="true"
                                               value="{{$data->address}}"
                                               aria-required="true"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Agama') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('religion') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('religion') ? ' is-invalid' : '' }}"
                                               name="religion" id="input-religion" type="text"
                                               placeholder="{{ __('Agama') }}"
                                               required="true"
                                               value="{{$data->religion}}"
                                               aria-required="true"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('RW') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('rw') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('rw') ? ' is-invalid' : '' }}"
                                               name="rw" id="input-rw" type="number"
                                               placeholder="{{ __('RW') }}"
                                               required="true"
                                               value="{{$data->rw}}"
                                               aria-required="true"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('RT') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('rt') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('rt') ? ' is-invalid' : '' }}"
                                               name="rt" id="input-rt" type="number"
                                               placeholder="{{ __('RT') }}"
                                               required="true"
                                               value="{{$data->rt}}"
                                               aria-required="true"/>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Jenis Kelamin') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('gender') ? ' has-danger' : '' }}">
                                        <select class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}"
                                                name="gender" id="input-gender" type="text"
                                                placeholder="{{ __('Jenis Kelamin') }}"
                                                required="true"
                                                aria-required="true">
                                            @if($data->gender == 'Laki-laki')
                                                <option value="Laki-laki" selected >Laki-laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                            @else
                                                <option value="Laki-laki">Laki-laki</option>
                                                <option value="Perempuan" selected>Perempuan</option>
                                            @endif
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <label class="col-sm-2 col-form-label">{{ __('Pekerjaan') }}</label>
                                <div class="col-sm-7">
                                    <div class="form-group{{ $errors->has('job_status') ? ' has-danger' : '' }}">
                                        <input class="form-control{{ $errors->has('job_status') ? ' is-invalid' : '' }}"
                                               name="job_status" id="input-job_status" type="text"
                                               placeholder="{{ __('Pekerjaan') }}"
                                               required="true"
                                               value="{{$data->job_status}}"
                                               aria-required="true"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer ml-auto mr-auto">
                            <div class="row">
                                <div class="col-sm-4 ml-auto mr-auto">
                                    <div class="card-footer ml-auto mr-auto">
                                        <button type="submit" class="btn btn-info">{{ __('Update') }}</button>
                                        <a href="{{route('warga.delete',['nik'=>$data->nik])}}">
                                            <button type="button" class="btn btn-danger">{{ __('Hapus') }}</button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
