@extends('layouts.app', ['activePage' => 'warga', 'titlePage' => __('Add a Citizen')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" action="{{ route('warga.add') }}" autocomplete="off"
                          class="form-horizontal">
                        @csrf
                        @method('post')

                        <input name="_id" value="3" hidden/>

                        <div class="card ">
                            <div class="card-header card-header-primary">
                                <h4 class="card-title">{{ __('Form Profil Warga') }}</h4>
                                <p class="card-category">{{ __('Informasi Warga') }}</p>
                            </div>
                            <div class="card-body ">
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('NIK') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('nik') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('nik') ? ' is-invalid' : '' }}"
                                                   name="nik" id="input-nik" type="number"
                                                   placeholder="{{ __('NIK') }}"
                                                   required="true"
                                                   aria-required="true"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('KK') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('kk') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('kk') ? ' is-invalid' : '' }}"
                                                   name="kk" id="input-kk" type="number"
                                                   placeholder="{{ __('KK') }}"
                                                   required="true"
                                                   aria-required="true"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Nama') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                   name="name" id="input-name" type="text"
                                                   placeholder="{{ __('Nama') }}"
                                                   required="true"
                                                   aria-required="true"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Tempat Lahir') }}</label>
                                    <div class="col-sm-3">
                                        <div class="form-group{{ $errors->has('place_of_birth') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('place_of_birth') ? ' is-invalid' : '' }}"
                                                   name="place_of_birth" id="input-place_of_birth" type="text"
                                                   placeholder="{{ __('Tempat Lahir') }}"
                                                   required="true"
                                                   aria-required="true"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group{{ $errors->has('Tanggal Lahir') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('birthday') ? ' is-invalid' : '' }}"
                                                   name="birthday" id="input-birthday" type="date"
                                                   placeholder="{{ __('Tanggal Lahir') }}"
                                                   required="true"
                                                   aria-required="true"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Alamat') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('address') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}"
                                                   name="address" id="input-address" type="text"
                                                   placeholder="{{ __('Alamat') }}"
                                                   required="true"
                                                   aria-required="true"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Agama') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('religion') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('religion') ? ' is-invalid' : '' }}"
                                                   name="religion" id="input-religion" type="text"
                                                   placeholder="{{ __('Agama') }}"
                                                   required="true"
                                                   aria-required="true"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('RW') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('rw') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('rw') ? ' is-invalid' : '' }}"
                                                   name="rw" id="input-rw" type="number"
                                                   placeholder="{{ __('RW') }}"
                                                   required="true"
                                                   aria-required="true"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('RT') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('rt') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('rt') ? ' is-invalid' : '' }}"
                                                   name="rt" id="input-rt" type="number"
                                                   placeholder="{{ __('RT') }}"
                                                   required="true"
                                                   aria-required="true"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Jenis Kelamin') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('gender') ? ' has-danger' : '' }}">
                                            <select class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}"
                                                   name="gender" id="input-gender" type="text"
                                                   placeholder="{{ __('Jenis Kelamin') }}"
                                                   required="true"
                                                   aria-required="true">
                                                <option value="Laki-laki">Laki-laki</option>
                                                <option value="Perempuan">Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="col-sm-2 col-form-label">{{ __('Pekerjaan') }}</label>
                                    <div class="col-sm-7">
                                        <div class="form-group{{ $errors->has('job_status') ? ' has-danger' : '' }}">
                                            <input class="form-control{{ $errors->has('job_status') ? ' is-invalid' : '' }}"
                                                   name="job_status" id="input-job_status" type="text"
                                                   placeholder="{{ __('Pekerjaan') }}"
                                                   required="true"
                                                   aria-required="true"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer ml-auto mr-auto">
                                <button type="submit" class="btn btn-primary">{{ __('Simpan') }}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
