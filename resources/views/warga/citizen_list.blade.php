@extends('layouts.app', ['activePage' => 'warga', 'titlePage' => __('Table List of Citizen')])

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <h4 class="card-title ">Tabel Warga</h4>
                            <p class="card-category"> Seluruh Data Warga !!!</p>
                            <a href="{{route('warga.form')}}">
                                <button class="btn btn-primary"
                                        style="position: absolute; top: 0; right: 0; margin: 20px; display: inline-block;">
                                    Tambah Warga
                                </button>
                            </a>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table">
                                    <thead class=" text-primary">
                                    <th>
                                        ID
                                    </th>
                                    <th>
                                        NIK
                                    </th>
                                    <th>
                                        KK
                                    </th>
                                    <th>
                                        NAME
                                    </th>
                                    <th>
                                        ADDRESS
                                    </th>
                                    <th>
                                        RT
                                    </th>
                                    <th>
                                        RW
                                    </th>
                                    <th>
                                        GENRE
                                    </th>
                                    <th>
                                        ACTION
                                    </th>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $value)
                                        <tr>
                                            <td>
                                                {{$value->_id}}
                                            </td>
                                            <td>
                                                {{$value->nik}}
                                            </td>
                                            <td>
                                                {{$value->kk}}
                                            </td>
                                            <td>
                                                {{$value->name}}
                                            </td>
                                            <td class="text-primary">
                                                {{$value->address}}
                                            </td>
                                            <td>
                                                {{$value->rt}}
                                            </td>
                                            <td>
                                                {{$value->rw}}
                                            </td>
                                            <td>
                                                {{$value->gender}}
                                            </td>
                                            <td>
                                                <a href="{{route('warga.show',['nik'=>$value->nik])}}">
                                                    <button class="btn btn-success">
                                                        Lihat
                                                    </button>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
