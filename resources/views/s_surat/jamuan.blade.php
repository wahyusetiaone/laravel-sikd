
<html>
<head>
<title>Surat Keterangan Tidak Mampu</title>
</head>

<body>
<div>
<div>
<p style="margin-top: 0pt; margin-left: 72pt; margin-bottom: 0pt; text-align: center; font-size: 22pt;"><span style="height: 0pt; text-align: left; display: block; position: absolute; z-index: -65536;"><img style="margin-top: 0pt; margin-left: -63.6pt; position: absolute;" src="http://127.0.0.1:8000/icon.png" alt="" width="72" height="92" /></span><strong><span style="font-family: 'Times New Roman';">PEMERINTAH KABUPATEN KARANGANYAR</span></strong></p>
<p style="margin-top: 0pt; margin-left: 72pt; margin-bottom: 0pt; font-size: 4pt;"><strong><span style="font-family: 'Times New Roman';">&nbsp;</span></strong></p>
<p style="margin-top: 0pt; margin-left: 72pt; margin-bottom: 0pt; text-align: center; font-size: 20pt;"><strong><span style="font-family: 'Times New Roman';">KECAMATAN JATEN</span></strong></p>
    <p style="margin-top: 0pt; margin-left: 72pt; margin-bottom: 0pt; text-align: center; font-size: 20pt;"><strong><span style="font-family: 'Times New Roman';">DESA DAGEN</span></strong></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: center; font-size: 13pt;"><strong><span style="font-family: 'Times New Roman';">&nbsp;</span></strong></p>
<p style=" text-align: center; margin-top: 5pt; margin-bottom: 0pt; border-top: 1.5pt solid #000000; border-bottom: 1.5pt solid #000000; padding-top: 1pt; padding-bottom: 1pt; font-size: 11pt;"><span style="font-family: 'Times New Roman';">Alamat : Jl. Mojo No. 01 Telp. 826783
Kode Pos 57771</span></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; font-size: 10pt;"><span style="font-family: 'Times New Roman';">&nbsp;</span></p>
</div>
<h1 style="margin-top: 0pt; margin-bottom: 0pt; page-break-after: avoid; font-size: 14pt;"><span style="font-family: 'Times New Roman'; font-weight: normal;">&nbsp;</span></h1>
<h1 style="margin-top: 0pt; margin-bottom: 0pt; text-align: center; page-break-after: avoid; font-size: 12pt;"><u><span style="font-family: Tahoma;">SURAT IZIN PERJAMUAN</span></u></h1>
    <?php
    $str = str_replace('-', '/', $data->format);
    $no = str_replace('X', $data->no_surat, $str);
    ?>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: center; font-size: 12pt;"><span style="font-family: Tahoma;">Nomor :{{$no}}</span></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: justify; font-size: 10pt;"><span style="font-family: 'Times New Roman';">&nbsp;</span></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">Yang bertanda tangan di bawah ini Kepala Kelurahan Dagen , menerangkan bahwa :</span></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">&nbsp;</span></p>
<p style="margin-top: 0pt; margin-left: 25.2pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">Nama</span><span style="width: 92.02pt; display: inline-block;">&nbsp;</span><span style="font-family: Tahoma;">: {{$data->name}}</span></p>
<p style="margin-top: 0pt; margin-left: 25.2pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">Tempat / Tanggal Lahir</span><span style="width: 6.01pt; display: inline-block;">&nbsp;</span><span style="font-family: Tahoma;">: Karanganyar, {{$data->birthday}}</span><span style="width: 10.36pt; display: inline-block;">&nbsp;</span></p>
<p style="margin-top: 0pt; margin-left: 25.2pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">Jenis Kelamin</span><span style="width: 54.35pt; display: inline-block;">&nbsp;</span><span style="font-family: Tahoma;">: {{$data->gender}}</span><span style="width: 10.36pt; display: inline-block;">&nbsp;</span></p>
<p style="margin-top: 0pt; margin-left: 25.2pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">Agama</span><span style="width: 86.69pt; display: inline-block;">&nbsp;</span><span style="font-family: Tahoma;">: Islam</span><span style="width: 10.36pt; display: inline-block;">&nbsp;</span></p>
    <p style="margin-top: 0pt; margin-left: 25.2pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">Nomor KTP</span><span style="width: 65.27pt; display: inline-block;">&nbsp;</span><span style="font-family: Tahoma;">:{{$data->nik}}</span><span style="width: 10.36pt; display: inline-block;">&nbsp;</span></p>
    <p style="margin-top: 0pt; margin-left: 25.2pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">Keperluan  </span><span style="width: 65.27pt; display: inline-block;">&nbsp;</span><span style="font-family: Tahoma;">: {{$data->keperluan}}</span><span style="width: 10.36pt; display: inline-block;">&nbsp;</span></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">&nbsp;</span></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;"></span></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">&nbsp;</span></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">Demikian Surat Ijin ini kami keluarkan bagi yang bersangkutan untuk menjadikan pegangan adanya.</span></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">&nbsp;</span></p>
<p style="margin-top: 0pt; margin-left: 25.65pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">&nbsp;</span></p>
<p style="margin-top: 0pt; margin-left: 25.65pt; float:right; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="width: 82.65pt; display: inline-block;">&nbsp;</span><span style="width: 17.1pt; display: inline-block;">&nbsp;</span><span style="width: 126.6pt; display: inline-block;">&nbsp;</span><span style="font-family: Tahoma;">Pracimantoro,</span><span style="font-family: Tahoma;">{{$data->dikeluarkan}}</span></p>
</br>
<p style="margin-top: 0pt; margin-left: 25.65pt; float:right; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="width: 126.6pt; display: inline-block;">&nbsp;</span><span style="font-family: Tahoma;">Kepala Kelurahan Dagen</span></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">&nbsp;</span></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">&nbsp;</span></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">&nbsp;</span></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">&nbsp;</span></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">&nbsp;</span></p>
<p style="margin-top: 0pt; margin-right: 25.65pt; float:right; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="width: 82.65pt; display: inline-block;">&nbsp;</span><span style="width: 17.1pt; display: inline-block;">&nbsp;</span><span style="width: 126.6pt; display: inline-block;">&nbsp;</span><strong><strong><span style="font-family: Tahoma;">_________________________</span></strong></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">&nbsp;</span></p>
    <p style="margin-top: 0pt; margin-right: 25.65pt; float:right; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="width: 82.65pt; display: inline-block;">&nbsp;</span><span style="width: 17.1pt; display: inline-block;">&nbsp;</span><span style="width: 126.6pt; display: inline-block;">&nbsp;</span><strong><strong><span style="font-family: Tahoma;">JAKA MARYANTO, S.H.</span></strong></p>
    <p style="margin-top: 0pt; margin-bottom: 0pt; text-align: justify; font-size: 11pt;"><span style="font-family: Tahoma;">&nbsp;</span></p>
<p style="margin-top: 0pt; margin-bottom: 0pt; font-size: 11pt;"><span style="font-family: Tahoma;">&nbsp;</span></p>
</div>
{{--<script>--}}
{{--    window.onload = function(){--}}
{{--        window.print();--}}
{{--    }--}}
{{--</script>--}}
</body>
</html>
